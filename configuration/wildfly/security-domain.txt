============
= General =
============

# Overlay the jboss-web.xml file
deployment-overlay add --name=sw --content=/WEB-INF/jboss-web.xml=configuration/wildfly/jboss-web.xml \
                       --deployments=msg.war --redeploy-affected

# Enable logs for security stuff
/subsystem=logging/logger=org.jboss.security:add(level=DEBUG)
/subsystem=logging/logger=org.picketbox:add(level=DEBUG)

============
= Database =
============

# JBoss CLI (OK on WildFly < 25 or JBoss EAP 6+)
/subsystem=security/security-domain=sw-domain:add(cache-type=default)
/subsystem=security/security-domain=sw-domain/authentication=classic:add()
/subsystem=security/security-domain=sw-domain/authentication=classic/login-module=Database         \
    :add(code=Database, flag=required,                                                             \
         module-options={"dsJndiName" => "java:jboss/datasources/SewaDS",                          \
                         "principalsQuery" => "select Passwd from SW_Users where UserID=?",        \
                         "rolesQuery" => "select RoleID, 'Roles' from SW_Roles where UserID=?"})

==============
= Properties =
==============

# JBoss CLI (OK on WildFly < 25 or JBoss EAP 6+)
/subsystem=security/security-domain=sw-domain:add(cache-type=default)
/subsystem=security/security-domain=sw-domain/authentication=classic:add()
/subsystem=security/security-domain=sw-domain/authentication=classic/login-module=UsersRoles    \
    :add(code=UsersRoles, flag=required,                                                        \
         module-options={"usersProperties"=>"${jboss.server.config.dir}/sw-users.properties",   \
                         "rolesProperties"=>"${jboss.server.config.dir}/sw-roles.properties"})

========
= LDAP =
========

# JBoss CLI (OK on WildFly < 25 or JBoss EAP 6+)
/subsystem=security/security-domain=sw-domain:add(cache-type=default)
/subsystem=security/security-domain=sw-domain/authentication=classic:add()
/subsystem=security/security-domain=sw-domain/authentication=classic/login-module=LdapExtended  \
    :add(code=LdapExtended, flag=required,                                                      \
         module-options={"java.naming.provider.url"=>"ldap://dirserver:1389",                   \
                         "bindDN"=>"cn=Root",                                                   \
                         "bindCredential"=>"RootPwd",                                           \
                         "baseCtxDN"=>"ou=people,dc=sewatech,dc=fr",                            \
                         "baseFilter"=>"(uid={0})",                                             \
                         "rolesCtxDN"=>"ou=groups,dc=sewatech,dc=fr",                           \
                         "roleFilter"=>"(uniqueMember={1})",                                    \
                         "roleAttributeID"=>"cn"})
