package fr.sewatech.formation.appserv;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageServiceJms;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JmsClient implements GenericCommand {

  private static final Logger logger = LogManager.getLogger(JmsClient.class);

  public static void main(String[] args) {
    Message message = new Message("Local message", null);

    try {
      new JmsClient().execute("", "send", "5");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @param args "?", commande, nombre de messages, delai
   * @throws Exception
   */
  @Override
  public void execute(String... args) throws Exception {
    System.out.println("Preparation... ");
    MessageServiceJms messageService = ServiceLocator.getServiceJms();

    String command;
    if (args.length > 1) {
      command = args[1];
    } else {
      command = "send";
    }

    int count;
    if (args.length > 2) {
      count = Integer.parseInt(args[2]);
    } else {
      count = 1;
    }

    int delay;
    if (args.length > 3) {
      delay = Integer.parseInt(args[3]);
    } else {
      delay = 1;
    }

    if ("receive".equals(command)) {
      System.out.println("Receiving... ");
      while (true) {
        Message receivedMessage = messageService.getMessage();
        System.out.println("Envoi de message " + receivedMessage);
        Thread.sleep(delay * 1000);
      }
    } else {
      System.out.println("Sending... ");
      for (int i = 0; i < count; i++) {
        Message sentMessage = new Message();
        sentMessage.setText("Message n°" + i);
        messageService.send(sentMessage);
        System.out.println("Envoi de message " + sentMessage);
        Thread.sleep(delay * 1000);
      }
    }
  }

}
