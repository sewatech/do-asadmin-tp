package fr.sewatech.formation.appserv;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EjbClient implements GenericCommand {

    private static final Logger logger = LogManager.getLogger(EjbClient.class);

    public static void main(String... args) {
        try {
            new EjbClient().execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(String... args) throws Exception {
        MessageService messageService = ServiceLocator.getMessageService();

        int messageId;
        if (args.length > 1) {
            messageId = Integer.parseInt(args[1]);
        } else {
            messageId = 0;
        }

        int count;
        if (args.length > 2) {
            count = Integer.parseInt(args[2]);
        } else {
            count = 1;
        }

        int delay;
        if (args.length > 3) {
            delay = Integer.parseInt(args[3]);
        } else {
            delay = 1;
        }

        for (int i = 0; i < count; i++) {
            Message message = messageService.getMessage(messageId);
            System.out.println("Appel " + i + " : " + message.getText() + " - "  + message.getOrigin());
            Thread.sleep(delay * 1_000L);
        }
    }

}
