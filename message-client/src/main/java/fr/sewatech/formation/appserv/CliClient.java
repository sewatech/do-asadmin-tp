package fr.sewatech.formation.appserv;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CliClient {
    private static final Logger logger = LogManager.getLogger(CliClient.class);

    public static void main(String[] args) {
        try {
            System.setProperty("javax.net.ssl.trustStore", "/opt/wildfly/standalone/configuration/application.keystore");
            System.setProperty("javax.net.ssl.trustStorePassword", "password");

//            InputStream props = Thread.currentThread().getContextClassLoader().getResourceAsStream("jboss-ejb-client.properties");
//            Properties clientProperties = new Properties();
//            clientProperties.load(props);
//            clientProperties.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
//            clientProperties.put("remote.connections", "default");
//            clientProperties.put("remote.connection.default.port", "8080");
//            clientProperties.put("remote.connection.default.host", "127.0.0.1");
//            clientProperties.put("remote.connection.default.username", "alexis");
//            clientProperties.put("remote.connection.default.password", "hassler");
//            clientProperties.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
//            clientProperties.put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS", "JBOSS-LOCAL-USER");
//
//            EJBClientConfiguration ejbClientConfiguration = new PropertiesBasedEJBClientConfiguration(clientProperties);
//            ContextSelector<EJBClientContext> contextSelector = new ConfigBasedEJBClientContextSelector(ejbClientConfiguration);
//            EJBClientContext.setSelector(contextSelector);

//            EJBClientContext.getSelector().getCurrent()
//                    .registerInterceptor(1, new EJBClientInterceptor() {
//                        @Override
//                        public void handleInvocation(EJBClientInvocationContext ejbClientInvocationContext) throws Exception {
//                            System.out.println("###################");
//                            EJBClientConfiguration conf = ejbClientInvocationContext.getClientContext().getEJBClientConfiguration();
//                            System.out.println(conf.getCallbackHandler());
//                            ejbClientInvocationContext.sendRequest();
//                        }
//
//                        @Override
//                        public Object handleInvocationResult(EJBClientInvocationContext ejbClientInvocationContext) throws Exception {
//                            return ejbClientInvocationContext.getResult();
//                        }
//                    });

            if (args.length > 0) {
                CliCommand.getCommandInstance(args[0]).execute(args);
            }
        } catch (Exception e) {
            logger.error("Houston, we have a problem...", e); 
        }
    }
}
