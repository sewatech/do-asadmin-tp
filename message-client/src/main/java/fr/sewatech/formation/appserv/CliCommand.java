package fr.sewatech.formation.appserv;

import java.lang.reflect.InvocationTargetException;

public enum CliCommand {
  ejb("ejb", EjbClient.class),
  ejbStateful("ejb-stateful", EjbStatefulClient.class),
  jms("jms", JmsClient.class);

  private final String arg;
  private final Class<? extends GenericCommand> clientClass;

  CliCommand(String arg, Class<? extends GenericCommand> clientClass) {
    this.arg = arg;
    this.clientClass = clientClass;
  }

  Class<? extends GenericCommand> getClientClass() {
    return clientClass;
  }

  static CliCommand getCommand(String arg) {
    for (CliCommand command : values()) {
      if (command.arg.equals(arg)) {
        return command;
      }
    }
    return ejb;
  }

  static Class<? extends GenericCommand> getCommandClass(String arg) {
    return getCommand(arg).getClientClass();
  }

  static GenericCommand getCommandInstance(String arg) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
    return getCommandClass(arg).getConstructor().newInstance();
  }

}
