package fr.sewatech.formation.appserv;

import fr.sewatech.formation.appserv.service.MessageService;
import fr.sewatech.formation.appserv.service.MessageServiceJms;
import org.apache.logging.log4j.LogManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;

public final class ServiceLocator {

    private static MessageServiceJms serviceJms = null;
    private static MessageService messageServiceEjb = null;
    private static MessageService messageStatefulEjb = null;

    static {
        try {
            Context namingContext = new InitialContext();

            Properties props = new Properties();
            URL url = Thread.currentThread().getContextClassLoader().getResource("jndi-names.properties");
            props.load(url.openStream());

            messageServiceEjb = (MessageService) namingContext.lookup(props.getProperty("ejb.MessageServiceBean"));
            messageStatefulEjb = (MessageService) namingContext.lookup(props.getProperty("ejb.MessageStatefulBean"));
            serviceJms = MessageServiceJms.unsecured();
//            serviceJms.setRemote(true);
//            serviceJms.setFactoryName(props.getProperty("jms.ConnectionFactory"));
//            serviceJms.setQueueName(props.getProperty("jms.SWq"));
//            serviceJms.setSecured(Boolean.valueOf(props.getProperty("jms.secured")));
        } catch (Exception ex) {
            LogManager.getLogger(ServiceLocator.class.getName()).error("", ex);
        }
    }

    private ServiceLocator() {
    }

    public static MessageService getMessageService() {
        return messageServiceEjb;
    }

    public static MessageService getMessageStateful() {
        return messageStatefulEjb;
    }

    public static MessageServiceJms getServiceJms() {
        return serviceJms;
    }

}
