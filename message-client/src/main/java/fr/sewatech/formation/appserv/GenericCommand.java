package fr.sewatech.formation.appserv;

public interface GenericCommand {

    public void execute(String... args) throws Exception;
}
