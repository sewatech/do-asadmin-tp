package fr.sewatech.exemple.jms;

import jakarta.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class AccessMessages {
    protected static final String FACTORY_NAME = "java:jboss/exported/RemoteConnectionFactory";
    protected static final String QUEUE_NAME = "queue/SewaQueue";

    protected ConnectionFactory connectionFactory;
    protected Queue queue;
    private Connection connection;
    protected Session session;

    protected AccessMessages() throws NamingException {
        Context jndiContext = new InitialContext();
        connectionFactory = (ConnectionFactory) jndiContext.lookup(FACTORY_NAME);
        queue = (Queue) jndiContext.lookup(QUEUE_NAME);
    }

    protected void killServer(String signal) throws InterruptedException {
        new Command().kill(signal);
        Thread.sleep(1000);
    }

    protected void openConnection() throws JMSException {
        connection = connectionFactory.createConnection();
        connection.start();
    }

    protected void openSession() throws JMSException {
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    protected void closeSession() throws JMSException {
        session.close();
    }

    private void closeConnection() throws JMSException {
        connection.close();
    }
}
