package fr.sewatech.exemple.jms;

import jakarta.jms.JMSException;
import jakarta.jms.MessageProducer;
import jakarta.jms.TextMessage;
import javax.naming.NamingException;

public class SendMessages extends AccessMessages {

    private MessageProducer producer = null;

    public static void main(String... args) {
        String message = "...";
        if (args.length > 0) {
            message = args[0];
        }

        int nbMessages = 1;
        if (args.length > 1) {
            nbMessages = Integer.parseInt(args[1]);
        }

        int killIndex = -1;
        if (args.length > 2) {
            killIndex = Integer.parseInt(args[2]);
        }

        String killSignal = "";
        if (args.length > 3) {
            killSignal = args[3];
        }

        try {
            SendMessages sendMessages = new SendMessages();

            sendMessages.openConnection();
            sendMessages.openSession();
            for (int i = 0; i < nbMessages; i++) {
                if (i == killIndex) {
                    sendMessages.killServer(killSignal);
                }
                System.out.println("Sending message : " + message + " - " + i);
                sendMessages.sendMessage(message + " - " + i);
                Thread.sleep(1000);
            }
            sendMessages.closeSession();
            sendMessages.openConnection();
        } catch (NamingException | JMSException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private SendMessages() throws NamingException {
        super();
    }

    @Override
    protected void openSession() throws JMSException {
        super.openSession();
        producer = session.createProducer(queue);
    }

    private void sendMessage(String message) throws JMSException {
        TextMessage textMessage = session.createTextMessage(message);
        producer.send(textMessage);
    }

}
