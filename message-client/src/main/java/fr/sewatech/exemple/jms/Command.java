package fr.sewatech.exemple.jms;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Command {

    public static void main(String... args) {
        new Command().kill("");
    }

    public void kill(String signal) {
        try {
            String killCommand = "kill " + signal + " `jps | grep HornetQBootstrapServer`";
            File commandFile = new File("command.sh");
            FileWriter fileWriter = new FileWriter(commandFile);
            fileWriter.write("#!/bin/bash \n" + killCommand);
            fileWriter.close();
            System.out.println(commandFile.setExecutable(true));
            Process killProcess = Runtime.getRuntime().exec("./command.sh");
            int exitValue = killProcess.waitFor();
            System.out.println("Sent command : " + killCommand + " => " + exitValue);
            InputStream inputStream = killProcess.getInputStream();
            if (inputStream.available() > 0) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                String line = "";
                while (line != null) {
                    System.out.println(line = reader.readLine());
                }
            }
            InputStream errorStream = killProcess.getErrorStream();
            if (errorStream.available() > 0) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(errorStream, StandardCharsets.UTF_8));
                String line = "";
                while (line != null) {
                    System.out.println(line = reader.readLine());
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            //commandFile.delete();
        }
    }

}
