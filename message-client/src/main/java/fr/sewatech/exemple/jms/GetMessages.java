package fr.sewatech.exemple.jms;

import jakarta.jms.JMSException;
import jakarta.jms.MessageConsumer;
import jakarta.jms.TextMessage;
import javax.naming.NamingException;

public class GetMessages extends AccessMessages {

    private MessageConsumer consumer = null;

    public static void main(String... args) {
        int killIndex = -1;
        if (args.length > 0) {
            killIndex = Integer.parseInt(args[0]);
        }

        String killSignal = "";
        if (args.length > 1) {
            killSignal = args[1];
        }

        try {
            GetMessages getMessages = new GetMessages();

            getMessages.openConnection();
            getMessages.openSession();
            String message;
            int nbMessages = 0;
            int index = 0;
            while ( (message = getMessages.getMessage()) != null ) {
                if (index++ == killIndex) {
                    getMessages.killServer(killSignal);
                }

                System.out.println(message);
                Thread.sleep(500);
            }
            getMessages.closeSession();
            getMessages.openConnection();

        } catch (NamingException | JMSException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected GetMessages() throws NamingException {
    }

    @Override
    protected void openSession() throws JMSException {
        super.openSession();
        consumer = session.createConsumer(queue);
    }

    private String getMessage() throws JMSException {
        TextMessage textMessage = (TextMessage)consumer.receive(1000);
        return textMessage==null ? null : textMessage.getText();
    }

}
