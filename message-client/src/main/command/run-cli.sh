#!/bin/sh
DIRNAME=`dirname "$0"`
. $DIRNAME/set-env.sh

java  -cp $JBOSS_HOME/bin/client/*:$JBOSS_HOME/modules/org/apache/log4j/main/*:message-client.jar:conf/ fr.sewatech.formation.appserv.CliClient $@
