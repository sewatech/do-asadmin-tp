#!/bin/sh
APP_HOME=`dirname $0`
HORNETQ_HOME=/opt/java/hornetq-2.1.2.Final
java -cp $HORNETQ_HOME/lib/*:$APP_HOME/message-client.jar fr.sewatech.exemple.jms.SendMessages $@
