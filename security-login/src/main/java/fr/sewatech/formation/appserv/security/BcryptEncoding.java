package fr.sewatech.formation.appserv.security;

import org.wildfly.security.password.Password;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.WildFlyElytronPasswordProvider;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;
import org.wildfly.security.password.util.ModularCrypt;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public final class BcryptEncoding {

  private static final Provider ELYTRON_PROVIDER = new WildFlyElytronPasswordProvider();

  private static final int ITERATION_COUNT = 10;

  private BcryptEncoding() {
  }

  public static void main(String... args) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException {
    String password = args[0];

    if (args.length > 1) {
      if (args[1].startsWith("$2")) {
        verifyPassword(password, args[1]);
      } else {
        encodePassword(password, Base64.getDecoder().decode(args[1]));
      }
    } else {
      encodePassword(password, generateRandomSalt());
    }
  }

  private static byte[] generateRandomSalt() {
    byte[] salt = new byte[BCryptPassword.BCRYPT_SALT_SIZE];
    SecureRandom random = new SecureRandom();
    random.nextBytes(salt);
    return salt;
  }

  private static void encodePassword(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
    IteratedSaltedPasswordAlgorithmSpec iteratedAlgorithmSpec = new IteratedSaltedPasswordAlgorithmSpec(ITERATION_COUNT, salt);
    EncryptablePasswordSpec encryptableSpec = new EncryptablePasswordSpec(password.toCharArray(), iteratedAlgorithmSpec);
    BCryptPassword original = (BCryptPassword) PasswordFactory
        .getInstance(BCryptPassword.ALGORITHM_BCRYPT, ELYTRON_PROVIDER)
        .generatePassword(encryptableSpec);

    Base64.Encoder encoder = Base64.getEncoder();
    System.out.println("Encoded Salt = " + encoder.encodeToString(salt).replaceAll("=", ""));
    System.out.println("Encoded Hash = " + encoder.encodeToString(original.getHash()).replaceAll("=", ""));

    System.out.println("Modular Hash = " + ModularCrypt.encodeAsString(original));
  }

  private static void verifyPassword(String password, String encodedPassword) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException {
    Password decodedPassword = ModularCrypt.decode(encodedPassword);

    PasswordFactory passwordFactory = PasswordFactory
        .getInstance(BCryptPassword.ALGORITHM_BCRYPT, ELYTRON_PROVIDER);
    BCryptPassword restored = (BCryptPassword) passwordFactory
        .translate(decodedPassword);

    System.out.println("Password verification = " + (passwordFactory.verify(restored, password.toCharArray()) ? "OK" : "fail"));
  }

}
