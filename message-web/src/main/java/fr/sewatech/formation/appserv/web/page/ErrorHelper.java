package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.SewaException;
import fr.sewatech.formation.appserv.util.FormatUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class ErrorHelper implements PageHelper {

    private final Logger logger = LogManager.getLogger(ErrorHelper.class);
    private final FormatUtil formatter = new FormatUtil();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Throwable actualThrowable = extractException((Throwable) request.getAttribute("jakarta.servlet.error.exception"));
        log(actualThrowable);
        request.setAttribute("message", getMessage(response, actualThrowable));
    }

    private void log(Throwable error) {
        if (error instanceof SewaException) {
            logger.error(error.getMessage(), error.getCause() == null ? error : error.getCause());
        } else if (error != null){
            logger.error("", error);
        }
    }

    private String getMessage(HttpServletResponse response, Throwable error) {
        if (error == null) {
            return "Erreur " + response.getStatus();
        } else {
            return getMessage(error).replaceAll("\n", "<br/>");
        }
    }

    private String getMessage(Throwable error) {
        Throwable throwable;
        if (error instanceof SewaException && (error.getCause() != null)) {
            throwable = error.getCause();
        } else {
            throwable = error;
        }

        return throwable.getMessage() == null ? throwable.getClass().getName() : throwable.toString();
    }

    private static Throwable extractException(Throwable throwable) {
        while ((throwable instanceof ServletException) && (throwable.getCause() != null)) {
            throwable = throwable.getCause();
        }
        return throwable;
    }

    @Override
    public String asText(HttpServletRequest request) {
        return formatter.arrayTextFormat(
            formatter.textFormat("error", request.getAttribute("message")),
            formatter.textFormat("complement", request.getAttribute("complement"))
        );
    }

    @Override
    public String asJson(HttpServletRequest request) {
        return formatter.arrayJsonFormat(
            formatter.jsonFormat("error", request.getAttribute("message")),
            formatter.jsonFormat("complement", request.getAttribute("complement"))
        );
    }

}
