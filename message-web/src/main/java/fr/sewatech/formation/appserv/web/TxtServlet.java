package fr.sewatech.formation.appserv.web;

import fr.sewatech.formation.appserv.web.page.CookieHelper;
import fr.sewatech.formation.appserv.web.page.CounterHelper;
import fr.sewatech.formation.appserv.web.page.DatabaseHelper;
import fr.sewatech.formation.appserv.web.page.EjbHelper;
import fr.sewatech.formation.appserv.web.page.GcHelper;
import fr.sewatech.formation.appserv.web.page.JmsHelper;
import fr.sewatech.formation.appserv.web.page.MemoryHelper;
import fr.sewatech.formation.appserv.web.page.MessageHelper;
import fr.sewatech.formation.appserv.web.page.PageHelper;
import fr.sewatech.formation.appserv.web.page.RequestHelper;
import fr.sewatech.formation.appserv.web.page.ServerHelper;
import fr.sewatech.formation.appserv.web.page.ThreadHelper;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = {"*.txt", "*.json"})
public class TxtServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final Map<String, PageHelper> helpers;

    public TxtServlet() {
        helpers = new HashMap<>();
        helpers.put("/message/simple", new MessageHelper());
        helpers.put("/message/database", new DatabaseHelper());
        helpers.put("/message/ejb", new EjbHelper());
        helpers.put("/message/messaging", new JmsHelper());
        helpers.put("/misc/server", new ServerHelper());
        helpers.put("/misc/count", new CounterHelper());
        helpers.put("/misc/memory", new MemoryHelper());
        helpers.put("/http/info", new RequestHelper());
        helpers.put("/http/cookie", new CookieHelper());
        helpers.put("/perf/threads", new ThreadHelper());
        helpers.put("/perf/gc", new GcHelper());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String servletPath = req.getServletPath();
        PageHelper helper = helpers.get(servletPath.substring(0, servletPath.lastIndexOf('.')));
        if (helper == null) {
            resp.sendError(404);
        } else {
            String extension = servletPath.substring(servletPath.lastIndexOf('.') + 1);
            switch (extension) {
                case "json":
                    resp.setContentType("application/json");
                    helper.prepare(req, resp);
                    resp.getWriter().print(helper.asJson(req));
                    break;
                case "txt":
                    resp.setContentType("text/plain");
                    helper.prepare(req, resp);
                    resp.getWriter().print(helper.asText(req));
                    break;
            }
        }
    }
}
