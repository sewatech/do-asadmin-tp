package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.ejb.MessageSessionBean;
import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import fr.sewatech.formation.appserv.util.JndiUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import javax.naming.NamingException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * @author Alexis Hassler
 */
public class CdiHelper implements PageHelper {

    private Logger logger = LogManager.getLogger(CdiHelper.class);

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        MessageService service = getMessageService();

        String id = request.getParameter("id");
        if (id == null) {
            id = "0";
        }
        Message message = service.getMessage(Integer.parseInt(id));

        request.setAttribute("id", id);
        request.setAttribute("count", service.countMessages());
        request.setAttribute("message", message);
    }

    private MessageService getMessageService() throws ServletException {
        MessageService service;
        try {
            BeanManager beanManager = JndiUtil.lookupBeanManager();

            Class<?> type = MessageSessionBean.class;
            Set<Bean<?>> beans = beanManager.getBeans(type);
            if (beans.isEmpty()) {
                throw new ServletException("Could not locate a bean of type " + type.getName());
            }
            Bean<?> bean = beanManager.resolve(beans);
            CreationalContext<?> context = beanManager.createCreationalContext(bean);
            service = (MessageSessionBean) beanManager.getReference(bean, bean.getBeanClass(), context);
        } catch (NamingException e) {
            throw new ServletException(e);
        }
        return service;
    }
}
