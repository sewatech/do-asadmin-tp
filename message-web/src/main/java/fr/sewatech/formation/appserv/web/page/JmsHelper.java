package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageServiceJms;
import fr.sewatech.formation.appserv.util.ClassUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class JmsHelper implements PageHelper {
    private MessageServiceJms securedService = MessageServiceJms.secured();
    private MessageServiceJms unsecuredService = MessageServiceJms.unsecured();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MessageServiceJms service;
        if (request.getServletPath().startsWith("/secured")) {
            service = securedService;
        } else {
            service = unsecuredService;
        }

        String operation = request.getParameter("operation");
        Message message = null;

        if ("send".equals(operation)) {
            String textMessage = request.getParameter("message");
            if (textMessage != null && !textMessage.isEmpty()) {
                message = new Message();
                message.setText(textMessage);
                message.setOrigin(ClassUtil.getLibrary(MessageServiceJms.class));
                service.send(message);
            }
        }

        if ("send".equals(operation)) {
            if (message == null) {
                request.setAttribute("sendResult", "Pas de message envoy&eacute;.");
            } else {
                request.setAttribute("sendResult", "Message envoy&eacute; : " + message.getText());
            }
        }

        if ("receive".equals(operation)) {
            message = service.getMessage();
            request.setAttribute("receiveResult", "Message re&ccedil;u : " + (message == null ? "" : message.getText()));
        }
    }
}
