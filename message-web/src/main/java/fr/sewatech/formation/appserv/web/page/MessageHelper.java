package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import fr.sewatech.formation.appserv.service.MessageServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * @author Alexis Hassler
 */
public class MessageHelper extends AbstractPageHelper {

    private final Logger logger = LogManager.getLogger(MessageHelper.class);
    private final MessageService service = new MessageServiceImpl();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (id == null) {
            id = "0";
        }
        Message message = new Message(service.getMessage(Integer.parseInt(id)));

        Principal userPrincipal = request.getUserPrincipal();
        if (userPrincipal != null) {
            message.setText(message.getText().replace("tout le monde", userPrincipal.getName()));
            logger.debug("Authenticated as " + userPrincipal.getName());
        }

        if (request.getServletPath().startsWith("/tls") && request.isSecure()) {
            message.setText(message.getText() + " (TLS)");
        }

        if (request.getServletPath().startsWith("/secured")) {
            String role = request.getParameter("role");
            request.setAttribute("inRole", request.isUserInRole(role));
        }

        request.setAttribute("id", id);
        request.setAttribute("count", service.countMessages());
        request.setAttribute("message", message);
    }
}
