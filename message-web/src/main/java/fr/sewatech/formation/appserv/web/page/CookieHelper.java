package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.util.FormatUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Alexis Hassler
 */
public class CookieHelper implements PageHelper {

  private final FormatUtil formatter = new FormatUtil();

  @Override
  public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  }

  @Override
  public String asText(HttpServletRequest request) {
    return formatter.arrayTextFormat(
        Arrays.stream(request.getCookies())
            .map(cookie -> formatter.textFormat(cookie.getName(), cookie.getValue()))
            .toArray(String[]::new)
    );
  }

  @Override
  public String asJson(HttpServletRequest request) {
    return formatter.arrayJsonFormat(
        Arrays.stream(request.getCookies())
            .map(cookie -> formatter.jsonFormat(cookie.getName(), cookie.getValue()))
            .toArray(String[]::new)
    );
  }
}
