package fr.sewatech.formation.appserv.web.page;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class FileSystemHelper implements PageHelper {
    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getParameter("action");
        if ("newfile".equals(action)) {
            File newfile = new File("nosecurity.txt");
            boolean ok = newfile.createNewFile();
            if (ok) {
                request.setAttribute("result", "Fichier &eacute;crit " + newfile.getAbsolutePath());
            }
        } else if ("deletefile".equals(action)) {
            File dir = new File(".");
            for (File file : dir.listFiles()) {
                if ("nosecurity.txt".equals(file.getName())) {
                    boolean ok = file.delete();
                    request.setAttribute("result", "Fichier supprim&eacute " + file.getAbsolutePath());
                }
            }

        }
    }
}
