package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.MessageService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class EjbHelper extends AbstractPageHelper {

    private MessageService securedService = null;
    private MessageService unsecuredService = null;

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (securedService == null) {
                securedService = InitialContext.doLookup("java:module/MessageSecuredBean!fr.sewatech.formation.appserv.ejb.MessageServiceLocal");
            }
            if (unsecuredService == null) {
                unsecuredService = InitialContext.doLookup("java:module/MessageServiceBean!fr.sewatech.formation.appserv.ejb.MessageServiceLocal");
            }
            MessageService service;
            if (request.getServletPath().startsWith("/secured")) {
                service = securedService;
            } else {
                service = unsecuredService;
            }

            String id = request.getParameter("id");
            if (id == null) {
                id = "0";
            }
            request.setAttribute("count", service.countMessages());
            request.setAttribute("id", id);
            request.setAttribute("message", service.getMessage(Integer.parseInt(id)));
        } catch (NamingException e) {
            throw new ServletException(e);
        }

    }
}
