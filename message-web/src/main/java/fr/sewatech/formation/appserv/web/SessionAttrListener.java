package fr.sewatech.formation.appserv.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionAttributeListener;
import jakarta.servlet.http.HttpSessionBindingEvent;

@WebListener
public class SessionAttrListener implements HttpSessionAttributeListener{
    private static Logger logger = LogManager.getLogger(SessionAttrListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        logger.debug("Sesion attribute added : " + event.getName());
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        logger.debug("Sesion attribute removed : " + event.getName());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        logger.debug("Sesion attribute replaced : " + event.getName());
    }
}
