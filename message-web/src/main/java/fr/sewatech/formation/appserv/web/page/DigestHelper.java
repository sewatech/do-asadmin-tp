package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.crypto.Digester;
import fr.sewatech.formation.appserv.crypto.Encoding;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SecureRandom;

import static fr.sewatech.formation.appserv.web.page.RequestUtil.getIntParameter;

/**
 * @author Alexis Hassler
 */
public class DigestHelper implements PageHelper {
    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = request.getParameter("message");
        if (message == null) message = "";
        request.setAttribute("message", message);

        Digester algo = Digester.fromText(request.getParameter("algo"));
        request.setAttribute("algo", algo.text());

        Encoding encoding = Encoding.fromText(request.getParameter("encoding"));
        request.setAttribute("encoding", encoding);

        int iterations = getIntParameter(request, "iterations", 1);
        int saltLength = getIntParameter(request, "salt-length", 0);
        byte[] salt = salt(saltLength);
        if (saltLength > 0) {
            request.setAttribute(
                    "result",
                    encoding.encode(salt)
                            + '$' + iterations + '$'
                            + encoding.encode(algo.digest(message, salt)));
        } else {
            request.setAttribute(
                    "result",
                    encoding.encode(algo.digest(message, salt)));
        }
    }

    private static byte[] salt(int saltLength) {
        byte[] salt = new byte[saltLength];
        new SecureRandom().nextBytes(salt);
        return salt;
    }

}
