package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.util.FormatUtil;

import jakarta.servlet.http.HttpServletRequest;

/**
 * @author Alexis Hassler
 */
public abstract class AbstractPageHelper implements PageHelper {

    private final FormatUtil formatter = new FormatUtil();

    @Override
    public String asText(HttpServletRequest request) {
        return formatter.arrayTextFormat(formatter.textFormat("message", request.getAttribute("message")));
    }

    @Override
    public String asJson(HttpServletRequest request) {
        return formatter.arrayJsonFormat(formatter.jsonFormat("message", request.getAttribute("message")));
    }
}
