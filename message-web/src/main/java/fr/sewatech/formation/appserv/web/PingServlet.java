package fr.sewatech.formation.appserv.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet(urlPatterns = "/ping")
public class PingServlet extends HttpServlet {
    private Logger logger = LogManager.getLogger(PingServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("GET");
        response.setContentType("text/plain");
        try {
            String delayAsText = request.getParameter("delay");
            if (delayAsText != null) {
                int delay = Integer.parseInt(delayAsText);
                if (delay >0) {
                    Thread.sleep(delay * 1_000);
                }
            }
            Writer out = response.getWriter();
            out.write("OK");
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}