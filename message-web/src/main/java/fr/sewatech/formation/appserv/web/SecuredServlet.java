package fr.sewatech.formation.appserv.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


/**
 * Add authentication and authorization to the /sercured sub-context
 */
@WebServlet(name = "SecuredServlet", urlPatterns = SecuredServlet.SECURED_PREFIX + "/*")
public class SecuredServlet extends HttpServlet {
    static final String SECURED_PREFIX = "/secured";
    private static final String NON_SECURED_PREFIX = "/message";
    private static final String ERROR_PAGE = "/error.jsp";
    private static final Set<String> SECURED_PAGES = new HashSet<>(Collections.singletonList(SECURED_PREFIX + "/simple.jsp"));

    private static final Logger logger = LogManager.getLogger(SecuredServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (checkAuthentication(request, response)) {
            String path = NON_SECURED_PREFIX + request.getPathInfo();

            RequestDispatcher dispatcher = request.getRequestDispatcher(path);
            dispatcher.include(request, response);
        }
    }

    private boolean checkAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return !SECURED_PAGES.contains(getPagePath(request))
                || authenticate(request, response)
                && authorize(request, response);
    }

    private String getPagePath(HttpServletRequest request) {
        return request.getServletPath() + request.getPathInfo();
    }

    private boolean authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.trace("Authentication required : " + request.getPathInfo());
        try {
            if (request.authenticate(response)) {
                logger.trace("Authentication success : " + request.getPathInfo());
                return true;
            } else {
                // Tomcat return false
                logger.warn("Authentication failed : " + request.getPathInfo());
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            }
        } catch (ServletException e) {
            // WildFly throws this exception
            logger.warn("Authentication failed : " + request.getPathInfo());
            response.setHeader("WWW-Authenticate", "Basic realm=\"Secured Web page\"");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
    }

    private boolean authorize(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.info("Request : " + request.getClass());
        if (request.isUserInRole("sw-webuser")) {
            logger.trace("Authorisation OK : " + request.getPathInfo());
            return true;
        } else {
            logger.warn("Authorisation denied : " + request.getPathInfo());
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return false;
        }
    }

}
