package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.perf.memory.GcBenchmarkMBean;
import fr.sewatech.formation.appserv.util.FormatUtil;
import fr.sewatech.formation.appserv.util.MBeanUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author Alexis Hassler
 */
public class GcHelper extends AbstractPageHelper {

    private final Logger logger = LogManager.getLogger(GcHelper.class);
    private final FormatUtil formatter = new FormatUtil();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        if ("GC".equals(action)) {
            long duration = MBeanUtil.getMBeanInstance("GcBenchmark", GcBenchmarkMBean.class).start();
            String message = "Done in " + duration + " ms";
            request.setAttribute("message", message);
        } else {
            request.setAttribute("message", "Ready");
        }
    }
}
