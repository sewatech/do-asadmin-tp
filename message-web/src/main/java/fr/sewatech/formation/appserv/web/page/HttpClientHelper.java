package fr.sewatech.formation.appserv.web.page;

import com.ning.http.client.AsyncHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author Alexis Hassler
 */
public class HttpClientHelper implements PageHelper {

    private Logger logger = LogManager.getLogger(HttpClientHelper.class);

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            String numberAsText = request.getParameter("number");
            if (numberAsText != null) {
                int number = Integer.parseInt(numberAsText);
                if (number > 0) {
                    String path = "http://localhost:" + request.getServerPort() + request.getContextPath() + "/ping";
                    AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                    for (int i = 0; i < number; i++) {
                        asyncHttpClient.prepareGet(path).addQueryParam("delay", request.getParameter("delay")).execute();
                    }
                    Thread.sleep(50);
                    asyncHttpClient.closeAsynchronously();
                }
            }
        } catch (InterruptedException e) {
            throw new ServletException(e);
        }

    }
}
