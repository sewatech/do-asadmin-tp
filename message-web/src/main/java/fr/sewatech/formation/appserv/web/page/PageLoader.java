package fr.sewatech.formation.appserv.web.page;

import jakarta.servlet.ServletException;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Alexis Hassler
 */
public class PageLoader {

    private static final PageLoader instance = new PageLoader();

    private final ConcurrentMap<String, PageHelper> cache = new ConcurrentHashMap<>();

    public PageHelper load(String name) throws ServletException {
        try {
            PageHelper helper = cache.get(name);
            if (helper == null) {
                Class<?> type = Class.forName(PageHelper.class.getPackage().getName() + "." + name);
                PageHelper newHelper = (PageHelper) type.getDeclaredConstructor().newInstance();
                PageHelper existingHelper = cache.putIfAbsent(name, newHelper);
                return (existingHelper == null ? newHelper : existingHelper);
            } else {
                return helper;
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            throw new ServletException(e);
        }
    }

    public static PageLoader instance() {
        return instance;
    }

}
