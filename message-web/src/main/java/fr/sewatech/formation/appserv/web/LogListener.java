package fr.sewatech.formation.appserv.web;

import fr.sewatech.formation.appserv.util.ClassUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.impl.Log4jContextFactory;
import org.apache.logging.log4j.spi.LoggerContextFactory;

import java.net.URI;

@WebListener
public class LogListener implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(LogListener.class);

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String configuration = servletContextEvent.getServletContext().getInitParameter("log4j.configuration");
        if (configuration == null || configuration.isEmpty()) {
            configuration = System.getProperty("log4j.configuration");
        }
        if (configuration == null || configuration.isEmpty()) {
            logger.info("No custom configuration for Log4J");
        } else {
            LoggerContextFactory logFactory = LogManager.getFactory();
            if (logFactory instanceof Log4jContextFactory) {
                logger.info("Trying to reconfigure Log4J with file " + configuration);
                Configurator.reconfigure(URI.create(configuration));
            } else {
                logger.info("Unable to reconfigure Log4J because it is not the regular one");
            }
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Undeploying context");
    }
}
