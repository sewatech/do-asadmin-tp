package fr.sewatech.formation.appserv.web.page;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Alexis Hassler
 */
public interface PageHelper {
  void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

  default String asText(HttpServletRequest request) {
    return this.toString();
  }

  default String asJson(HttpServletRequest req) {
    return "{}";
  }

  static String textFormat(String... values) {
    return format("\n", values);
  }

  static String jsonFormat(String... values) {
    return '{' +
        format(",", values) +
        '}';

  }

  static String format(String separator, String... values) {
    String[] formatParts = new String[values.length];
    Arrays.fill(formatParts, "%s");
    return String.format(
        String.join(separator, formatParts),
        values);
  }

}
