package fr.sewatech.formation.appserv.web.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class RedirectHelper implements PageHelper {

    private Logger logger = LogManager.getLogger(RedirectHelper.class);

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String url = request.getParameter("url");
        if (url == null || url.isEmpty()) {
            url = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getServerPort()
                + request.getContextPath() + "/";

            String page = request.getParameter("page");
            if (page != null) {
                url += page;
            }
            request.setAttribute("url", url);
        } else {
            response.sendRedirect(url);
        }

//        response.setHeader("Refresh", "3; url=" + url);
//        response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    }
}
