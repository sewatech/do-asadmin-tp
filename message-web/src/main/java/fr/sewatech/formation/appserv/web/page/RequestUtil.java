package fr.sewatech.formation.appserv.web.page;

import jakarta.servlet.http.HttpServletRequest;

class RequestUtil {
    static int getIntParameter(HttpServletRequest request, String param, int defaultValue) {
        try {
            String parameter = request.getParameter(param);
            request.setAttribute("salt-length", parameter);
            return Integer.parseInt(parameter);
        } catch (Exception e) {
            return defaultValue;
        }
    }

}
