package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.Counter;
import fr.sewatech.formation.appserv.util.BeanUtil;
import fr.sewatech.formation.appserv.util.FormatUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * @author Alexis Hassler
 */
public class CounterHelper implements PageHelper {

  private final Logger logger = LogManager.getLogger(CounterHelper.class);

  private final BeanUtil beanUtil = new BeanUtil();
  private final FormatUtil formatter = new FormatUtil();

  @Override
  public void prepare(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession();
    Counter cnt = (Counter) session.getAttribute("count");

    if (cnt == null) {
      cnt = new Counter();
      logger.debug("New counter created");
    }

    cnt.increment();
    session.setAttribute("count", cnt);
    logger.debug("Count:" + cnt.getNumber());
  }

  @Override
  public String asText(HttpServletRequest request) {
    HttpSession session = request.getSession();
    return formatter.arrayTextFormat(beanUtil.property2Text(session.getAttribute("count"), "number"));
  }

  @Override
  public String asJson(HttpServletRequest request) {
    HttpSession session = request.getSession();
    return formatter.arrayJsonFormat(beanUtil.property2Json(session.getAttribute("count"), "number"));
  }

}
