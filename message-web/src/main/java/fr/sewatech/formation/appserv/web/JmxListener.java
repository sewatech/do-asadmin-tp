package fr.sewatech.formation.appserv.web;

import fr.sewatech.formation.appserv.perf.memory.GcBenchmark;
import fr.sewatech.formation.appserv.perf.memory.MemoryManaged;
import fr.sewatech.formation.appserv.service.CounterMBean;
import fr.sewatech.formation.appserv.service.MessageManaged;
import jakarta.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import static fr.sewatech.formation.appserv.util.MBeanUtil.registerMBean;
import static fr.sewatech.formation.appserv.util.MBeanUtil.unregisterMBean;

@WebListener
public class JmxListener implements ServletContextListener {
    private final Logger logger = LogManager.getLogger(JmxListener.class);
    @Inject
    private CounterMBean counterMBean;
    @Inject
    private MemoryManaged memoryManaged;
    @Inject
    private MessageManaged messageManaged;
    @Inject
    private GcBenchmark gcBenchmark;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.debug("Registering MBeans");
        if (counterMBean == null ) counterMBean = new CounterMBean();
        registerMBean(counterMBean, "Counter");
        if (memoryManaged == null) memoryManaged = new MemoryManaged();
        registerMBean(memoryManaged, "Memory");
        if (messageManaged == null) messageManaged = new MessageManaged();
        registerMBean(messageManaged, "Message");
        if (gcBenchmark == null) gcBenchmark = new GcBenchmark();
        registerMBean(gcBenchmark, "GcBenchmark");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.debug("Unregistering MBeans");
        unregisterMBean("Counter");
        unregisterMBean("Memory");
        unregisterMBean("Message");
        unregisterMBean("GcBenchmark");
    }
}
