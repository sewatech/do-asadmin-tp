package fr.sewatech.formation.appserv.web.page;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoggingHelper implements PageHelper {
    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = request.getParameter("message");
        String loggerName = request.getParameter("logger");
        String level = request.getParameter("level");

        if (message != null
                && loggerName != null
                && level != null) {
            Logger logger = LogManager.getLogger(loggerName);
            logger.log(Level.toLevel(level), message);
        }
    }
}
