package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.util.BeanUtil;
import fr.sewatech.formation.appserv.util.FormatUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class RequestHelper implements PageHelper {

  private static final String REQUEST_ATTRIBUTE_NAME = "requestInfo";

  private final FormatUtil formatter = new FormatUtil();
  private final BeanUtil beanUtil = new BeanUtil();

  @Override
  public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  }

  @Override
  public String asText(HttpServletRequest request) {
    return formatter.arrayTextFormat(
        beanUtil.property2Text(request, "userPrincipal"),
        beanUtil.property2Text(request, "contextPath"),
        beanUtil.property2Text(request, "servletPath"),
        beanUtil.property2Text(request, "requestURL"),
        beanUtil.property2Text(request, "pathInfo"),
        beanUtil.property2Text(request, "pathTranslated"),
        beanUtil.property2Text(request, "localAddr"),
        beanUtil.property2Text(request, "remoteAddr"),
        beanUtil.property2Text(request, "remoteUser"),
        beanUtil.property2Text(request, "serverName"),
        beanUtil.property2Text(request, "serverPort"),
        beanUtil.property2Text(request.getSession(), "id")
    );
  }

  @Override
  public String asJson(HttpServletRequest request) {
    return formatter.arrayJsonFormat(
        beanUtil.property2Json(request, "userPrincipal"),
        beanUtil.property2Json(request, "contextPath"),
        beanUtil.property2Json(request, "servletPath"),
        beanUtil.property2Json(request, "requestURL"),
        beanUtil.property2Json(request, "pathInfo"),
        beanUtil.property2Json(request, "pathTranslated"),
        beanUtil.property2Json(request, "localAddr"),
        beanUtil.property2Json(request, "remoteAddr"),
        beanUtil.property2Json(request, "remoteUser"),
        beanUtil.property2Json(request, "serverName"),
        beanUtil.property2Json(request, "serverPort"),
        beanUtil.property2Json(request.getSession(), "id")
    );
  }
}
