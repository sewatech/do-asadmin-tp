package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.perf.thread.Deadlock;
import fr.sewatech.formation.appserv.perf.thread.Threading;
import fr.sewatech.formation.appserv.util.FormatUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author Alexis Hassler
 */
public class ThreadHelper extends AbstractPageHelper {

    private final Logger logger = LogManager.getLogger(ThreadHelper.class);
    private final FormatUtil formatter = new FormatUtil();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Threading threading = new Threading();
        String action = request.getParameter("action");
        if ("Sleep".equals(action)) {
            threading.sleep();
            request.setAttribute("message", "Done " + action);
        } else if ("Park".equals(action)) {
            threading.park();
            request.setAttribute("message", "Done " + action);
        } else if ("Park (timeout)".equals(action)) {
            threading.parkWithTimeout();
            request.setAttribute("message", "Done " + action);
        } else if ("Lock".equals(action)) {
            threading.lock();
            request.setAttribute("message", "Done " + action);
        } else if ("Sync".equals(action)) {
            threading.sync();
            request.setAttribute("message", "Done " + action);
        } else if ("Deadlock".equals(action)) {
            Deadlock.start();
            request.setAttribute("message", "Done " + action);
        } else {
            request.setAttribute("message", "Ready to start some threads.");
        }
    }
}
