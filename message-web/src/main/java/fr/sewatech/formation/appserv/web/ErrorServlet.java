package fr.sewatech.formation.appserv.web;

import fr.sewatech.formation.appserv.web.page.ErrorHelper;
import fr.sewatech.formation.appserv.web.page.PageHelper;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/error"})
public class ErrorServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private final PageHelper helper = new ErrorHelper();

  public ErrorServlet() {
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String contentType = response.getContentType();
    helper.prepare(request, response);

    if (contentType == null) {
      request.getRequestDispatcher("/error.jsp").forward(request, response);
      return;
    }

    switch (contentType) {
      case "application/json":
        response.getWriter().print(helper.asJson(request));
        break;
      case "text/plain":
        response.getWriter().print(helper.asText(request));
        break;
      default:
        request.getRequestDispatcher("/error.jsp").forward(request, response);
        break;
    }
  }

}
