package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.ServerInfo;
import fr.sewatech.formation.appserv.service.ServerService;
import fr.sewatech.formation.appserv.util.BeanUtil;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class ServerHelper implements PageHelper {

  private static final String REQUEST_ATTRIBUTE_NAME = "serverInfo";

  private final ServerService serverService = new ServerService();
  private final BeanUtil beanUtil = new BeanUtil();

  @Override
  public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute(REQUEST_ATTRIBUTE_NAME, serverService.currentServer());
  }

  @Override
  public String asText(HttpServletRequest request) {
    ServerInfo bean = (ServerInfo) request.getAttribute(REQUEST_ATTRIBUTE_NAME);
    return beanUtil.property2Text(bean, "OsName") +
        beanUtil.property2Text(bean, "OsVersion") +
        beanUtil.property2Text(bean, "OsArch") +
        beanUtil.property2Text(bean, "Processors") + '\n' +
        beanUtil.property2Text(bean, "VmName") +
        beanUtil.property2Text(bean, "VmVersion") + '\n' +
        beanUtil.property2Text(bean, "AppServerName") +
        beanUtil.property2Text(bean, "AppServerVersion") + '\n' +
        beanUtil.property2Text(bean, "Hostname") +
        beanUtil.property2Text(bean, "ProcessId");
  }

  @Override
  public String asJson(HttpServletRequest request) {
    ServerInfo bean = (ServerInfo) request.getAttribute(REQUEST_ATTRIBUTE_NAME);
    return String.format("{%s, %s, %s, %s, %s, %s, %s, %s, %s, %s}",
        beanUtil.property2Json(bean, "OsName"),
        beanUtil.property2Json(bean, "OsVersion"),
        beanUtil.property2Json(bean, "OsArch"),
        beanUtil.property2Json(bean, "Processors"),
        beanUtil.property2Json(bean, "VmName"),
        beanUtil.property2Json(bean, "VmVersion"),
        beanUtil.property2Json(bean, "AppServerName"),
        beanUtil.property2Json(bean, "AppServerVersion"),
        beanUtil.property2Json(bean, "Hostname"),
        beanUtil.property2Json(bean, "ProcessId"));
  }
}
