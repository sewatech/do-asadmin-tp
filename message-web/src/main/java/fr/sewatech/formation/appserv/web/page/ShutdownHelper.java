package fr.sewatech.formation.appserv.web.page;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author Alexis Hassler
 */
public class ShutdownHelper implements PageHelper {
    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) {
        String action = request.getParameter("action");
        if ("stop".equals(action)) {
            System.exit(0);
        }
    }
}
