package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.perf.memory.MemoryManagedMBean;
import fr.sewatech.formation.appserv.util.BeanUtil;
import fr.sewatech.formation.appserv.util.FormatUtil;
import fr.sewatech.formation.appserv.util.MBeanUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.management.MalformedObjectNameException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class MemoryHelper implements PageHelper {

    private final Logger logger = LogManager.getLogger(MemoryHelper.class);
    private final BeanUtil beanUtil = new BeanUtil();
    private final FormatUtil formatter = new FormatUtil();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        MemoryManagedMBean memory = MBeanUtil.getMBeanInstance("Memory", MemoryManagedMBean.class);
        try {
            if ("+".equals(request.getParameter("action"))) {
                memory.plus();
            } else if ("-".equals(request.getParameter("action"))) {
                memory.minus();
            }
            request.setAttribute("count", memory.getCount());
            request.setAttribute("lastDuration", memory.getLastDuration());
            request.setAttribute("totalDuration", memory.getTotalDuration());
        } catch (OutOfMemoryError oome) {
            request.setAttribute("complement", "Heavy count : " + memory.getCount());
            throw oome;
        }
    }


    @Override
    public String asText(HttpServletRequest request) {
        MemoryManagedMBean memory = MBeanUtil.getMBeanInstance("Memory", MemoryManagedMBean.class);
        return formatter.arrayTextFormat(
            beanUtil.property2Text(memory, "count"),
            beanUtil.property2Text(memory, "weight"),
            beanUtil.property2Text(memory, "lastDuration"),
            beanUtil.property2Text(memory, "totalDuration")
        );
    }

    @Override
    public String asJson(HttpServletRequest request) {
        MemoryManagedMBean memory = MBeanUtil.getMBeanInstance("Memory", MemoryManagedMBean.class);
        return formatter.arrayJsonFormat(
            beanUtil.property2Json(memory, "count"),
            beanUtil.property2Json(memory, "weight"),
            beanUtil.property2Json(memory, "lastDuration"),
            beanUtil.property2Json(memory, "totalDuration")
        );
    }
}
