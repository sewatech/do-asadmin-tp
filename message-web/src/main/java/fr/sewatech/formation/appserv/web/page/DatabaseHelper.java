package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.service.MessageService;
import fr.sewatech.formation.appserv.service.MessageServiceDbImpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class DatabaseHelper extends AbstractPageHelper {
    private static final String noCloseAction = "OK (no close)";

    private MessageService serviceWithClose = MessageServiceDbImpl.withClose();
    private MessageService serviceWithoutClose = MessageServiceDbImpl.withoutClose();

    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        if (id == null) {
            id = "0";
        }
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }

        MessageService service;
        switch (action) {
            case noCloseAction:
                service = serviceWithoutClose;
                break;
            default:
                service = serviceWithClose;
                break;
        }

        request.setAttribute("count", serviceWithClose.countMessages());
        request.setAttribute("id", id);
        request.setAttribute("message", service.getMessage(Integer.parseInt(id)));
    }
}
