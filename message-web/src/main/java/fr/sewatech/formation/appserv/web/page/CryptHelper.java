package fr.sewatech.formation.appserv.web.page;

import fr.sewatech.formation.appserv.crypto.Crypter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Alexis Hassler
 */
public class CryptHelper implements PageHelper {
    @Override
    public void prepare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = request.getParameter("message");
        if (message == null) message = "";

        String encrypted = request.getParameter("encrypted");;
        if (encrypted == null) encrypted = "";

        String action = request.getParameter("action");
        if (action == null) action = "";

        Crypter crypter = new Crypter();
        if (action.equals("Encrypt")) {
            request.setAttribute("encrypted", crypter.encode(message));
        } else {
            request.setAttribute("message", crypter.decode(encrypted));
        }

    }
}
