<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="ServerHelper"/>

<t:layout>
    <table class="table table-striped">
        <caption>Informations du serveur</caption>
        <tr>
            <td>System</td>
            <td>${serverInfo.osName}, version ${serverInfo.osVersion}, ${serverInfo.osArch} with ${serverInfo.processors} processors</td>
        </tr>
        <tr>
            <td>Java</td>
            <td>${serverInfo.vmName}, version ${serverInfo.vmVersion}</td>
        </tr>
        <tr>
            <td>Application Server</td>
            <td>${serverInfo.appServerName} ${serverInfo.appServerVersion}</td>
        </tr>
        <tr>
            <td>Instance (route)</td>
            <td>${serverInfo.instanceName}</td>
        </tr>
        <tr>
            <td>Process</td>
            <td>pid ${serverInfo.processId} @ ${serverInfo.hostname}</td>
        </tr>
    </table>
</t:layout>
