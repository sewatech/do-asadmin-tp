<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="MemoryHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-1">
                <input type="submit" name="action" value="+" class="form-control"/>
            </div>
            <div class="col-sm-1">
                <input type="submit" name="action" value="-" class="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3">
                <div class="alert alert-success" role="alert">Heavy count : ${count}</div>
            </div>
        </div>
	</form>

</t:layout>