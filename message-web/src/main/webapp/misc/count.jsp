<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="CounterHelper"/>

<t:layout>
    <div class="form-horizontal">
        <div class="form-group" >
            <div class="col-sm-3">
                <div class="alert alert-success" role="alert">Appel n&bull; ${count.number}</div>
            </div>
        </div>
    </div>
</t:layout>
