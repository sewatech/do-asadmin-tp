<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="LoggingHelper" />

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-2">
                <span>Message</span>
            </div>
            <div class="col-sm-5">
                <input type="text" name="message" value="${param['message']}" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <span>Logger</span>
            </div>
            <div class="col-sm-5">
                <input type="text" name="logger" value="${param['logger']}" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <label>Level</label>
            </div>
            <div class="col-sm-5">
                <select name="level" class="form-control">
                    <option value="ERROR" ${param['level'] == 'ERROR' ? "selected" : ""}>ERROR</option>
                    <option value="WARN" ${param['level'] == 'WARN' ? "selected" : ""}> WARN</option>
                    <option value="INFO" ${param['level'] == 'INFO' ? "selected" : ""}> INFO</option>
                    <option value="DEBUG" ${param['level'] == 'DEBUG' ? "selected" : ""}>DEBUG</option>
                    <option value="TRACE" ${param['level'] == 'TRACE' ? "selected" : ""}>TRACE</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <input type="submit" value="Log" name="action" class="form-control"/>
            </div>
        </div>
    </form>
</t:layout>
