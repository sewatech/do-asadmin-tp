<%@ tag description="Result Tag" pageEncoding="UTF-8"%>
<%@ attribute name="result" type="java.lang.String" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:if condition="${!empty result}">
    <div>
        <span class="alert alert-success" role="alert">${result}</span>
    </div>
</t:if>
