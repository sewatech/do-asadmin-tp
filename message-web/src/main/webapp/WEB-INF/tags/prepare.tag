<%@ tag description="If Tag" pageEncoding="UTF-8"%>
<%@ attribute name="with" type="java.lang.String" %>

<%@ tag import="fr.sewatech.formation.appserv.web.page.PageLoader" %>

<% PageLoader.instance().load(with).prepare(request, response); %>