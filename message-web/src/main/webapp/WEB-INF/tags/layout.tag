<%@ tag description="Layout Tag" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html dir='ltr' lang='fr'>
<head>
    <title>Formation JBoss / Tomcat</title>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/css/bootstrap.min.css' type='text/css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/css/custom.css' type='text/css'>
    <link rel='icon' href='${pageContext.request.contextPath}/favicon.ico'/>
</head>
<body>
<div class="container">
    <div class="row">
        <t:menu root="${pageContext.request.contextPath}"/>
    </div>

    <div class="row">
        <jsp:doBody/>
    </div>

</div>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>
