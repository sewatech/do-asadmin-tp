<%@ tag description="If Tag" pageEncoding="UTF-8"%>
<%@ attribute name="condition" type="java.lang.Boolean" %>

<% if (condition) { %>
    <jsp:doBody/>
<% } %>
