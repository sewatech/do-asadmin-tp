<%@ tag description="Message Tag" pageEncoding="UTF-8"%>
<%@ attribute name="message" type="fr.sewatech.formation.appserv.service.Message" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<form>
    <t:message-dropdown count="3" id=""/>
    <input type="submit" value="OK" class="button"/>
</form>

<t:result result="${message.text}" />
