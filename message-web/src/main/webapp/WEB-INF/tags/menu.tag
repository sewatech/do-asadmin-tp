<%@ tag description="Menu Tag" pageEncoding="UTF-8" %>
<%@ attribute name="root" type="java.lang.String" %>

<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar" role="navigation">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Messages <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${root}/message/simple.jsp">Simple</a></li>
                    <li><a href="${root}/message/database.jsp">Database</a></li>
                    <li><a href="${root}/message/ejb.jsp">EJB</a></li>
                    <li><a href="${root}/message/messaging.jsp">JMS</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Authentication <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${root}/secured/simple.jsp">Web restricted</a></li>
                    <li><a href="${root}/secured/ejb.jsp">EJB restricted</a></li>
                    <%--<li><a href="${root}/secured/messaging.jsp">Secured JMS</a></li>--%>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Security <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${root}/tls/simple.jsp">Confidential (TLS)</a></li>
                    <li><a href="${root}/security/shutdown.jsp">Shutdown</a></li>
                    <li><a href="${root}/security/filesystem.jsp">File system</a></li>
                    <li><a href="${root}/security/digest.jsp">Digest</a></li>
                    <li><a href="${root}/security/crypt.jsp">En/Decrypt</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Performances <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${root}/misc/memory.jsp">Memory</a></li>
                    <li><a href="${root}/perf/threads.jsp">Threads</a></li>
                    <li><a href="${root}/perf/gc.jsp">GC</a></li>
                    <li><a href="${root}/perf/http-client.jsp">HTTP Client</a></li>
                    <%--<li><a href="${root}/perf/db-connection.jsp">DB</a></li>--%>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Misc <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${root}/misc/count.jsp">Count (session)</a></li>
                    <li><a href="${root}/http/info.jsp">Web Informations</a></li>
                    <li><a href="${root}/misc/server.jsp">Server Informations</a></li>
                    <li><a href="${root}/http/redirect.jsp">Redirect</a></li>
                    <li><a href="${root}/misc/logging.jsp">Logging</a></li>
                    <li><a href="${root}/http/cookie.jsp">Cookie</a></li>
                    <%--<li><a href="${root}/module-web.jsp">SLF4J (web)</a></li>--%>
                    <%--<li><a href="${root}/module-lib.jsp">SLF4J (lib)</a></li>--%>
                </ul>
            </li>
        </ul>
    </div>
</nav>
