<%@ tag description="Message Dropdown Tag" pageEncoding="UTF-8"%>
<%@ attribute name="id" type="java.lang.String" %>
<%@ attribute name="count" type="java.lang.Integer" %>

<div class="col-sm-2">
    <select name="id" class="form-control" >
    <%for (int i = 0; i <= count-1; i++) {
            String selected = (id.equals(String.valueOf(i)) ? "selected='selected'" : ""); %>
    <option value="<%= i %>" <%= selected %>>n&deg;<%= i %></option>
    <% } %>
</select>
</div>