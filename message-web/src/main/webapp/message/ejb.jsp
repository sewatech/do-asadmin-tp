<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="EjbHelper" />

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <t:message-dropdown count="${count}" id="${id}"/>
            <div class="col-sm-2">
                <input type="submit" value="OK" class="form-control"/>
            </div>
        </div>
        <div class="form-group" style="margin-top: 20px" >
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">${message.text}</div>
            </div>
        </div>

        <t:if condition="${pageContext.request.servletPath.startsWith('/secured')}">
            <div class="form-group">
                <div class="col-sm-5">
                    <input type="text" name="role" value="${role}" class="form-control" />
                </div>
                <div class="col-sm-2">
                    <input type="submit" value="Check role" class="form-control" />
                </div>
            </div>
            <t:if condition="${!empty param['role']}">
                <div class="form-group" >
                    <div class="col-sm-12">
                        <div class="alert alert-success" role="alert">You are ${inRole ? "" : "<b>not</b>"} in role : ${param['role']}</div>
                    </div>
                </div>
            </t:if>

        </t:if>
    </form>
</t:layout>
