<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="JmsHelper" />

<t:layout>
    <table width="100%">
        <tr>
            <td width="50%" valign="top">
                <form class="form-horizontal">
                    <div class="form-group" >

                        <input type="hidden" name="operation" value="send"/>
                        <div class="col-sm-8">
                            <input type="text" name="message" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <input type="submit" value="Send" class="form-control" />
                        </div>
                    </div>
                    <t:if condition="${!empty sendResult}">
                    <div class="form-group" >
                        <div class="col-sm-10">
                            <div class="alert alert-success" role="alert">${sendResult}</div>
                        </div>
                    </div>
                    </t:if>
                </form>
            </td>
            <td width="50%" valign="top">
                <form class="form-horizontal">
                    <div class="form-group" >
                        <input type="hidden" name="operation" value="receive"/>
                        <div class="col-sm-2">
                            <input type="submit" value="Receive" class="form-control" />
                        </div>
                    </div>
                    <t:if condition="${!empty receiveResult}">
                        <div class="form-group" >
                            <div class="col-sm-10">
                                <div class="alert alert-success" role="alert">${receiveResult}</div>
                            </div>
                        </div>
                    </t:if>
                </form>
            </td>
        </tr>
    </table>
</t:layout>
