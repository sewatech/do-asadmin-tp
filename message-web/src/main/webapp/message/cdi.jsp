<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:prepare with="CdiHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <t:message-dropdown count="${count}" id="${id}"/>
            <div class="col-sm-2">
                <input type="submit" value="OK" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">${message.text}</div>
            </div>
        </div>
    </form>
</t:layout>
