<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>

<t:layout>
    <div class="alert alert-danger" role="alert">
        ${message}
    </div>

    <c:if test="${!empty complement}">
        <div class="warning alert-warning" role="warning">
            ${complement}
        </div>
    </c:if>
</t:layout>
