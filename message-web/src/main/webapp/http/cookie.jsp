<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:layout>
    <table class="table table-striped">
    <caption>Cookies</caption>
        <c:forEach items="${cookie}" var="currentCookie">
            <tr>
                <td>${currentCookie.value.name}</td>
                <td>${currentCookie.value.value}</td>
            </tr>
        </c:forEach>
    </table>
</t:layout>
