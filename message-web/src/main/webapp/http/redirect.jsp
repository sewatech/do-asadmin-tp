<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:prepare with="RedirectHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="col-sm-6">
            <input type="text" name="url" value="${url}" class="form-control" readonly="readonly"/>
        </div>
        <div class="col-sm-2">
            <input type="submit" name="action" value="Redirect" class="form-control"/>
        </div>
    </form>
</t:layout>
