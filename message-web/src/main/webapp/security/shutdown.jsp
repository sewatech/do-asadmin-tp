<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="ShutdownHelper"/>

<t:layout>
	<form class="form-horizontal">
		<div class="form-group">
			<input type="hidden" name="action" value="stop" >
			<div class="col-sm-2">
				<input type="submit" value="Arr&ecirc;ter le serveur" class="form-control"/>
			</div>
		</div>
	</form>
</t:layout>
