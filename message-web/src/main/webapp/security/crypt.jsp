<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="CryptHelper" />

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-5">
                <input type="text" name="message" value="${message}" class="form-control"/>
            </div>
            <div class="col-sm-2">
                <input type="submit" value="Encrypt" class="form-control" name="action"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-5">
                <input type="text" name="encrypted" value="${encrypted}" class="form-control"/>
            </div>
            <div class="col-sm-2">
                <input type="submit" value="Decrypt" class="form-control" name="action"/>
            </div>
        </div>
    </form>
</t:layout>
