<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="DigestHelper" />

<t:layout>
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-5">
                <input type="text" name="message" value="${message}" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2">
                <input type="radio" name="algo" value="MD5" ${algo == 'MD5' ? "checked" : ""}>
                MD5
            </label>
            <label class="col-sm-2">
                <input type="radio" name="algo" value="SHA1" ${algo == 'SHA1' ? "checked" : ""}>
                SHA1
            </label>
            <label class="col-sm-2">
                <input type="radio" name="algo" value="SHA-256" ${algo == 'SHA-256' ? "checked" : ""}>
                SHA-256
            </label>
            <label class="col-sm-2">
                <input type="radio" name="algo" value="SHA-512" ${algo == 'SHA-512' ? "checked" : ""}>
                SHA-512
            </label>
        </div>
        <div class="form-group">
            <label class="col-sm-2">
                <input type="radio" name="encoding" value="hex" ${encoding == 'HEX' ? "checked" : ""}>
                Hex
            </label>
            <label class="col-sm-2">
                <input type="radio" name="encoding" value="base64" ${encoding == 'BASE64' ? "checked" : ""}>
                Base 64
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <input type="submit" value="Digest" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <t:if condition="${!empty result}">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">${result}</div>
            </div>
            </t:if>
        </div>
    </form>
</t:layout>