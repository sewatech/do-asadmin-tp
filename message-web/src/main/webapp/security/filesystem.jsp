<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="FileSystemHelper"/>

<t:layout>
    <form class="form-horizontal">

        <div class="form-group">
            <div class="col-sm-4">
                <div class="radio">
                <label>
                    <input type="radio" name="action" value="newfile">
                    Cr&eacute;er le fichier
                </label>
                </div>
                <div class="radio">
                <label>
                    <input type="radio" name="action" value="deletefile">
                    Supprimer le fichier
                </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <input type="submit" value="Go" class="form-control"/>
            </div>
        </div>

        <t:if condition="${!empty result}">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="alert alert-success" role="alert">${result}</div>
                </div>
            </div>
        </t:if>

    </form>
</t:layout>
