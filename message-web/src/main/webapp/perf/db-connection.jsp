<%@ page import="fr.sewatech.formation.appserv.perf.db.Connections" %>
<form>
    <label>Nombre de connexions</label>
    <input type="text" name="number" value="1"/>
    <input type="submit" value="OK" class="button"/>
</form>

<%!
    private Connections connections = new Connections("java:comp/env/jdbc/sewa-ds", true, 0);
%>

<%
    String numberAsText = request.getParameter("number");
    if (numberAsText != null) {
        connections.hold(Integer.parseInt(numberAsText));
    }

%>
