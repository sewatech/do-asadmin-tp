<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="ThreadHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="form-group" >
            <div class="col-sm-2">
                <input type="submit" name="action" value="Sleep" class="form-control" />
            </div>
            <div class="col-sm-2">
                <input type="submit" name="action" value="Park" class="form-control" />
            </div>
            <div class="col-sm-2">
                <input type="submit" name="action" value="Park (timeout)" class="form-control" />
            </div>
            <div class="col-sm-2">
                <input type="submit" name="action" value="Lock" class="form-control" />
            </div>
            <div class="col-sm-2">
                <input type="submit" name="action" value="Sync" class="form-control" />
            </div>
            <div class="col-sm-2">
                <input type="submit" name="action" value="Deadlock" class="form-control" />
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">${message}</div>
            </div>
        </div>
    </form>
</t:layout>
