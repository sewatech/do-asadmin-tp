<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="HttpClientHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="form-group" >
            <div class="col-sm-3">
                <label for="number" >Nombre de requ&ecirc;tes</label>
            </div>
            <div class="col-sm-3">
                <input type="text" id="number" name="number" value="1" class="form-control"/>
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-3">
                <label for="delay">Délai (s)</label>
            </div>
            <div class="col-sm-3">
                <input type="text" id="delay" name="delay" value="0" class="form-control"/>
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-3">
            </div>
            <div class="col-sm-3">
                <input type="submit" value="OK" class="form-control"/>
            </div>
        </div>
    </form>
</t:layout>
