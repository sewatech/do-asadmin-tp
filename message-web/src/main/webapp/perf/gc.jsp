<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:prepare with="GcHelper"/>

<t:layout>
    <form class="form-horizontal">
        <div class="form-group" >
            <div class="col-sm-2">
                <input type="submit" name="action" value="GC" class="form-control" />
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">${message}</div>
            </div>
        </div>
    </form>
</t:layout>
