package fr.sewatech.formation.appserv.micro;

import fr.sewatech.formation.appserv.util.JndiUtil;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;

@ApplicationScoped
@Liveness
public class DatabaseHealthChecker implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder builder = HealthCheckResponse.named("msg.database");
        try {
            return builder
                    .status(checkConnection("SewaDS"))
                    .build();
        } catch (SQLException | NamingException exception) {
            return builder
                    .down()
                    .withData("exception", exception.getClass().getName())
                    .withData("message", exception.getMessage())
                    .build();
        }
    }

    private boolean checkConnection(String ds) throws SQLException, NamingException {
        try (Connection connection = JndiUtil
                .lookupDataSource(ds)
                .getConnection()) {
            return connection
                    .isValid(5);
        }
    }
}
