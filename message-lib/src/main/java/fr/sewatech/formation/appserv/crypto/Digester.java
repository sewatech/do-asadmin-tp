package fr.sewatech.formation.appserv.crypto;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static fr.sewatech.formation.appserv.crypto.Encoding.DEFAULT_CHARSET;

/**
 * @author Alexis Hassler
 */
public enum Digester {
    MD5, SHA1, SHA_256, SHA_512;

    private static final Logger logger = LogManager.getLogger(Digester.class);

    public static Digester fromText(String text) {
        if (text == null) {
            return values()[0];
        }
        try {
            return valueOf(text.replace('-', '_').toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.warn("Unknown encoding : " + text);
            return values()[0];
        }
    }

    public String text() {
        return this.name().replace('_', '-');
    }

    public byte[] digest(String message) {
        return digest(message, new byte[0]);
    }

    public byte[] digest(String message, byte[] salt) {
        byte[] messageAsBytes = message.getBytes(DEFAULT_CHARSET);
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(this.text());
            if (salt.length > 0) {
                messageDigest.update(salt);
            }
            messageDigest.update(messageAsBytes);
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            logger.warn("Cannot digest message with algo " + this, e);
            return messageAsBytes;
        }
    }

}
