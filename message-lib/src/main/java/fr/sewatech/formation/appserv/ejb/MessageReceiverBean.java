package fr.sewatech.formation.appserv.ejb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;

/**
 * Message-Driven Bean implementation class for: MessageReceiverBean
 *
 */
//@MessageDriven(
//		activationConfig = { @ActivationConfigProperty(
//				propertyName = "destinationType", propertyValue = "jakarta.jms.Queue"
//		) }, 
//		mappedName = "queue/sewa")
public class MessageReceiverBean implements MessageListener {
	private static Logger logger = LogManager.getLogger(MessageReceiverBean.class);
	
    /**
     * Default constructor. 
     */
    public MessageReceiverBean() {
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
        	logger.info("Message reçu");
        	MessageHolder.add((fr.sewatech.formation.appserv.service.Message) message.getObjectProperty("message"));
		} catch (JMSException e) {
			logger.warn(e);
		}
    }
}
