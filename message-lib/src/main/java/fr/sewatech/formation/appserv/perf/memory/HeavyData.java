package fr.sewatech.formation.appserv.perf.memory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HeavyData {

  private final Logger log = LogManager.getLogger(HeavyData.class);

  private String str;
  private final List<Long> lg;

  public HeavyData(int size) {
    lg = new ArrayList<>();
    StringBuilder strBuf = new StringBuilder(size);
    Random rd = new Random();
    for (int i = 0; i < size; i++) {
      strBuf.append(rd.nextInt(0x186a0));
      lg.add(1_000L + i);
    }
    str = strBuf.toString();
  }

  public HeavyData interned() {
    str = str.intern();
    return this;
  }
}
