package fr.sewatech.formation.appserv.service;

/**
 * @author Alexis Hassler
 */
public class ServerInfo {

  private String instanceName = null;
  private String appServerName = null;
  private String appServerVersion = null;
  private String processId = null;
  private String vmName = null;
  private String vmVersion = null;
  private String osName = null;
  private String osArch = null;
  private String osVersion = null;
  private int processors = 0;
  private String hostname = null;

  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  public String getAppServerName() {
    return appServerName;
  }

  public void setAppServerName(String name) {
    this.appServerName = name;
  }

  public void setAppServerVersion(String version) {
    this.appServerVersion = version;
  }

  public String getAppServerVersion() {
    return appServerVersion;
  }

  public void setProcessId(String processId) {
    this.processId = processId;
  }

  public String getProcessId() {
    return processId;
  }

  public void setVmName(String vmName) {
    this.vmName = vmName;
  }

  public String getVmName() {
    return vmName;
  }

  public void setVmVersion(String vmVersion) {
    this.vmVersion = vmVersion;
  }

  public String getVmVersion() {
    return vmVersion;
  }

  public void setOsName(String osName) {
    this.osName = osName;
  }

  public String getOsName() {
    return osName;
  }

  public void setOsArch(String osArch) {
    this.osArch = osArch;
  }

  public String getOsArch() {
    return osArch;
  }

  public void setOsVersion(String osVersion) {
    this.osVersion = osVersion;
  }

  public String getOsVersion() {
    return osVersion;
  }

  public void setProcessors(int processors) {
    this.processors = processors;
  }

  public int getProcessors() {
    return processors;
  }

  public void setHostname(String hostname) {
    this.hostname = hostname;
  }

  public String getHostname() {
    return hostname;
  }

}
