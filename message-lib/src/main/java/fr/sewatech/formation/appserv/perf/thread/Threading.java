package fr.sewatech.formation.appserv.perf.thread;

import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Alexis Hassler
 */
public class Threading {

  public static final long TIMEOUT = 60_000L;

  public void sleep() {
    new Thread(() -> {
      try {
        Thread.sleep(TIMEOUT);
        log(Thread.currentThread());
      } catch (InterruptedException ignored) {
      }
    }, "sw-sleep")
        .start();
  }


  public void park() {
    new Thread(() -> {
      LockSupport.park();
      log(Thread.currentThread());
    }, "sw-park")
        .start();
  }

  public void parkWithTimeout() {
    new Thread(() -> {
      LockSupport.parkNanos(TIMEOUT * 1_000_000L);
      log(Thread.currentThread());
    }, "sw-park-to")
        .start();
  }

  public void lock() {
    final ReentrantLock lock = new ReentrantLock();

    for (int i = 0; i < 2; i++) {
      new Thread(() -> {
        try {
          lock.lock();
          Thread.sleep(TIMEOUT);
        } catch (InterruptedException ignored) {
        }
        log(Thread.currentThread());
      }, "sw-lock-" + i)
          .start();
    }
  }

  public void sync() {
    for (int i = 0; i < 2; i++) {
      new Thread(() -> {
        callSynchronized();
        log(Thread.currentThread());
      }, "sw-sync-" + i)
          .start();
    }
  }

  private void log(Thread thread) {
    System.out.println("End of thread " + thread.getName());
  }

  private synchronized void callSynchronized() {
    try {
      Thread.sleep(TIMEOUT);
    } catch (InterruptedException ignored) {
    }
  }

  public static void main(String... args) {
    try {
      Threading threading = new Threading();
      threading.sleep();
      threading.park();
      threading.parkWithTimeout();
      threading.lock();
      threading.sync();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        Thread.sleep(600_000);
      } catch (InterruptedException ignored) {
      }
    }
  }

}
