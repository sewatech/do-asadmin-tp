package fr.sewatech.formation.appserv.perf.thread;

import java.util.concurrent.locks.LockSupport;

public final class Deadlock {

  private Deadlock() {
  }

  static class Guy {
    private final String name;

    Guy(String name) {
      this.name = name;
    }

    public synchronized void ask(Guy friend) {
      System.out.println(this.name + " : Hi " + friend.name);
      LockSupport.parkNanos(10 * 1_000_000);
      friend.answer(this);
    }

    public synchronized void answer(Guy friend) {
      System.out.println(this.name + " : Ho " + friend.name + " !");
    }
  }

  public static void main(String... args) {
    start();
  }

  public static void start() {
    final Guy gaston = new Guy("Gaston");
    final Guy hilarion = new Guy("Hilarion");

    new Thread(() -> gaston.ask(hilarion), "deadlock-0").start();

    new Thread(() -> hilarion.ask(gaston), "deadlock-1").start();
  }

}
