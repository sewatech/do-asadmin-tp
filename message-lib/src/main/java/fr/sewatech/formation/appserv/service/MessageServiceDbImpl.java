package fr.sewatech.formation.appserv.service;

import fr.sewatech.formation.appserv.util.ClassUtil;
import fr.sewatech.formation.appserv.util.JndiUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageServiceDbImpl implements MessageService {

  private final Logger logger = LogManager.getLogger(MessageServiceDbImpl.class);

  private final boolean close;

  public static MessageServiceDbImpl withClose() {
    return new MessageServiceDbImpl(true);
  }

  public static MessageServiceDbImpl withoutClose() {
    return new MessageServiceDbImpl(false);
  }

  private MessageServiceDbImpl(boolean close) {
    logger.trace("Close = " + close);
    this.close = close;
  }

  public Message getMessage(int messageId) {
    logger.debug("Demande du message n° " + messageId);
    Connection cx = null;
    try {
      cx = getConnection();
      PreparedStatement st = cx.prepareStatement("select message from Message where id=?");
      st.setInt(1, messageId);
      ResultSet rs = st.executeQuery();
      Message message = null;
      if (rs.next()) {
        message = new Message();
        message.setText(rs.getString(1));
        message.setOrigin(ClassUtil.getLibrary(this.getClass()));
        logger.debug("Réponse : " + message);
      } else {
        logger.debug("Pas de réponse");
      }
      return message;
    } catch (Exception e) {
      throw new SewaException("Error while getting message " + messageId, e);
    } finally {
      close(cx, close);
    }
  }

  public int addMessage(String text) {
    logger.debug("Ajout du message " + text);
    Connection cx = null;
    try {
      cx = getConnection();

      int messageId = 0;
      PreparedStatement stId = cx
          .prepareStatement("select max(id) from Message");
      ResultSet rsId = stId.executeQuery();
      if (rsId.next()) {
        messageId = rsId.getInt(1);
      }

      PreparedStatement st = cx
          .prepareStatement("insert into Message (id, message) values (?, ?)");
      st.setInt(1, messageId);
      st.setString(2, text);
      st.executeUpdate();

      return messageId;

    } catch (Exception e) {
      throw new SewaException("Error while adding messages", e);
    } finally {
      close(cx, close);
    }
  }

  public int countMessages() {
    logger.debug("Demande du nombre de messages");
    Connection cx = null;
    try {
      cx = getConnection();
      PreparedStatement st = cx.prepareStatement("select count(*) from Message");
      ResultSet rs = st.executeQuery();
      rs.next();

      int count = rs.getInt(1);
      logger.debug("Réponse : " + count);
      return count;
    } catch (Exception e) {
      throw new SewaException("Error while counting messages", e);
    } finally {
      close(cx, true);
    }
  }

  private Connection getConnection() throws NamingException, SQLException {
    return JndiUtil
        .lookupDataSource("SewaDS")
        .getConnection();
  }

  private void close(Connection cx, boolean close) {
    try {
      if (cx != null && close) {
        logger.debug("Demande de fermeture de la connexion " + cx.hashCode());
        cx.close();
      }
    } catch (SQLException e) {
      logger.warn("Problème à la fermeture de la connexion", e);
    }
  }
}
