package fr.sewatech.formation.appserv.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.as.version.ProductConfig;
import org.jboss.modules.ModuleLoader;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

/**
 * @author Alexis Hassler
 */
public class ServerService {

  private final Logger logger = LogManager.getLogger(ServerService.class);

  private boolean notOnJBoss = false;

  public ServerInfo currentServer() {
    ServerInfo serverInfo = new ServerInfo();

    withOperatingSystemInfo(serverInfo);

    withRuntimeInfo(serverInfo);

    withJBossInfo(serverInfo);
    withTomcatInfo(serverInfo);

    return serverInfo;
  }

  private void withRuntimeInfo(ServerInfo serverInfo) {
    RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
    String[] splittedName = runtime.getName().split("@");
    serverInfo.setProcessId(splittedName[0]);
    serverInfo.setHostname(splittedName[1]);
    serverInfo.setVmName(runtime.getVmName());

    serverInfo.setVmVersion(System.getProperty("java.version"));
  }

  private void withOperatingSystemInfo(ServerInfo serverInfo) {
    OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
    serverInfo.setOsName(os.getName());
    serverInfo.setOsArch(os.getArch());
    serverInfo.setOsVersion(os.getVersion());
    serverInfo.setProcessors(os.getAvailableProcessors());
  }

  private void withJBossInfo(ServerInfo serverInfo) {
    if (notOnJBoss) {
      return;
    }

    try {
      ProductConfig config = ProductConfig.fromFilesystemSlot(
          ModuleLoader.forClassLoader(this.getClass().getClassLoader()),
          System.getProperty("jboss.home.dir"),
          null
      );

      serverInfo.setInstanceName(System.getProperty("jboss.node.name"));
      serverInfo.setAppServerName(config.getProductName());
      serverInfo.setAppServerVersion(config.getProductVersion());
      logger.debug("JBoss server information found");
    } catch (NoClassDefFoundError oups) {
      notOnJBoss = true;
      logger.info("Not on JBoss, trying Catalina domain");
    }
  }

  private void withTomcatInfo(ServerInfo serverInfo) {
    if (!notOnJBoss) {
      return;
    }

    try {
      String[] info = org.apache.catalina.util.ServerInfo.getServerInfo().split("/");
      serverInfo.setAppServerName(info[0]);
      serverInfo.setAppServerVersion(info[1]);

      MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
      ObjectName name = new ObjectName("Catalina:type=Engine");
      serverInfo.setInstanceName((String) mBeanServer.getAttribute(name, "jvmRoute"));

      logger.debug("Tomcat server information found");
    } catch (NoClassDefFoundError | JMException oups) {
      logger.warn("Not on JBoss, neither on Tomcat");
    }
  }

}
