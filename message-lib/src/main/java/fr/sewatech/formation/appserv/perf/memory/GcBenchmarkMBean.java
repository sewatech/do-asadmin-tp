package fr.sewatech.formation.appserv.perf.memory;

public interface GcBenchmarkMBean {

    long start();

    int getThreadsNum();

    void setThreadsNum(int threadsNum);

    int getOldObjectsNum();

    void setOldObjectsNum(int oldObjectsNum);

    int getObjectSize();

    void setObjectSize(int objectSize);

    long getIterations();

    void setIterations(long iterations);

}