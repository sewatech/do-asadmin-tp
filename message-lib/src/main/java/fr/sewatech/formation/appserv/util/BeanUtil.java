package fr.sewatech.formation.appserv.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BeanUtil {

    private final FormatUtil formatter = new FormatUtil();

    public String property2Json(Object bean, String propertyName) {
        return formatter.jsonFormat(propertyName, get(bean, propertyName));
    }

    public String property2Text(Object bean, String propertyName) {
        Object propertyValue = get(bean, propertyName);
        return formatter.textFormat(propertyName, get(bean, propertyName));
    }

    private Method getter(Object bean, String propertyName) throws NoSuchMethodException {
        return bean.getClass().getDeclaredMethod("get" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1));
    }

    private Object get(Object bean, String propertyName) {
        try {
            return getter(bean, propertyName).invoke(bean);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return "N/A";
        }
    }

}
