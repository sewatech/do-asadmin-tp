package fr.sewatech.formation.appserv.ejb;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import fr.sewatech.formation.appserv.service.MessageServiceImpl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
public class MessageSessionBean implements MessageService, Serializable {
    private final static Logger logger = LogManager.getLogger(MessageSessionBean.class);

    private List<Message> messages = new ArrayList<>();

    @PostConstruct
    public void init() {
        MessageService delegate = new MessageServiceImpl();
        for (int i = 0; i < delegate.countMessages(); i++) {
            Message message = delegate.getMessage(i);
            message.setText(message.getText() + " (via CDI)");
            messages.add(message);
        }
    }

    public Message getMessage(int id) {
        logger.info("Getting message with id " + id);
        return messages.get(id);
    }

    public int countMessages() {
        return messages.size();
    }

    public int addMessage(String text) {
        Message message = new Message();
        message.setText(text);
        messages.add(message);
        return messages.size() - 1;
    }
}
