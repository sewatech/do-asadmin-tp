package fr.sewatech.formation.appserv.service;

import fr.sewatech.formation.appserv.util.ClassUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MessageServiceImpl implements MessageService {

  private final File origin = ClassUtil.getLibrary(MessageServiceImpl.class);
  private final List<Message> messages;

  public MessageServiceImpl() {
    messages = new ArrayList<>();

    messages.add(new Message("Hello tout le monde", origin));
    messages.add(new Message("Salut everybody", origin));
    messages.add(new Message("JAAS is the way", origin));
  }

  @Override
  public Message getMessage(int id) {
    return messages.get(id);
  }

  @Override
  public int countMessages() {
    return messages.size();
  }

  @Override
  public int addMessage(String text) {
    messages.add(new Message(text, origin));
    return messages.size() - 1;
  }

}
