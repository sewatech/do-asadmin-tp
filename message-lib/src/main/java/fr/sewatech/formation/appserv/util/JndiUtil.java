package fr.sewatech.formation.appserv.util;

import fr.sewatech.formation.appserv.service.SewaException;

import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Alexis Hassler
 */
public final class JndiUtil {

    private static final String JBOSS_DATASOURCE_PREFIX = "java:jboss/datasources/";
    private static final String DEFAULT_DATASOURCE_PREFIX = "java:comp/env/jdbc/";
//    private static final String CONNECTION_FACTORY_NAME = "java:jboss/exported/jms/RemoteConnectionFactory";
    private static final String CONNECTION_FACTORY_NAME = "java:/ConnectionFactory";
    private static final String QUEUE_PREFIX = "java:/queue/";

    private static final InitialContext JNDI_CONTEXT;

    static {
        try {
            JNDI_CONTEXT = new InitialContext();
        } catch (Exception e) {
            throw new SewaException(e);
        }
    }

    private JndiUtil() {
    }

    public static DataSource lookupDataSource(String name) throws NamingException {
        String datasourceName = (inJBoss() ? JBOSS_DATASOURCE_PREFIX : DEFAULT_DATASOURCE_PREFIX) + name;
        return (DataSource) JNDI_CONTEXT.lookup(datasourceName);
    }

    public static ConnectionFactory lookupConnectionFactory() throws NamingException {
        return (ConnectionFactory) JNDI_CONTEXT.lookup(CONNECTION_FACTORY_NAME);
    }

    public static Queue lookupQueue(String name) throws NamingException {
        return (Queue) JNDI_CONTEXT.lookup(QUEUE_PREFIX + name);
    }

    public static BeanManager lookupBeanManager() throws NamingException {
        return (BeanManager) JNDI_CONTEXT.lookup("java:comp/BeanManager");
    }

    private static boolean inJBoss() {
        try {
            JNDI_CONTEXT.lookup("java:jboss/");
            return true;
        } catch (NamingException e) {
            return false;
        }
    }

}
