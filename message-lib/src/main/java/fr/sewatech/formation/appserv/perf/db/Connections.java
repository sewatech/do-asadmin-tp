package fr.sewatech.formation.appserv.perf.db;

import fr.sewatech.formation.appserv.service.SewaException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class Connections {

    private final Logger logger = LogManager.getLogger(Connections.class);

    private final Context namingContext;
    private final String dataSourceName;
    private final boolean close;
    private final int delay;
    private final Collection<Connection> connections = new ArrayList<>();


    public Connections(String dataSourceName, boolean close, int delay) {
        this.dataSourceName = dataSourceName;
        this.close = close;
        this.delay = delay;
        try {
            namingContext = new InitialContext();
        } catch (NamingException e) {
            throw new SewaException(e);
        }
    }

    public void hold(int number) {
        if (number <= 0) {
            return;
        }
        try {
            for (int i = 0; i < number; i++) {
                connections.add(getConnection());
            }
        } catch (NamingException | SQLException e) {
            throw new SewaException(String.format("Holding %s connections", number), e);
        }
    }
    public void release() {
        for (Connection connection : connections) {
            close(connection);
        }
    }

    Connection getConnection() throws NamingException, SQLException {
        DataSource ds = (DataSource) namingContext.lookup(dataSourceName);
        return ds.getConnection();
    }

    void close(Connection cx) {
        try {
            if (cx != null && close) {
                logger.debug("Demande de fermeture de la connexion " + cx.hashCode() + " avec un délai de " + delay + " s");
                if (delay > 0) {
                    try {
                        Thread.sleep(delay * 1_000L);
                    } catch (InterruptedException ignored) {
                    }
                }
                cx.close();
            }
        } catch (SQLException e) {
            logger.warn("Problème à la fermeture de la connexion", e);
        }
    }

}
