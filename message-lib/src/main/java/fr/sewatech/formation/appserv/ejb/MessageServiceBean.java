package fr.sewatech.formation.appserv.ejb;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.annotation.security.PermitAll;
import jakarta.ejb.Local;
import jakarta.ejb.Remote;
import jakarta.ejb.Stateless;

@Stateless
@Local(MessageServiceLocal.class)
@Remote(MessageService.class)
@PermitAll
public class MessageServiceBean implements MessageService {
    private static final Logger logger = LogManager.getLogger(MessageServiceBean.class);

	@Override
	public Message getMessage(int id) {
        logger.info("Getting message with id " + id);
		return MessageHolder.get(id);
	}

	@Override
	public int countMessages() {
		return MessageHolder.count();
	}

	@Override
	public int addMessage(String text) {
		Message message = new Message();
		message.setText(text);
		return MessageHolder.add(message);
	}
}
