package fr.sewatech.formation.appserv.perf.memory;

import fr.sewatech.formation.appserv.service.SewaException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.eclipse.microprofile.metrics.annotation.Timed;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GcBenchmark implements GcBenchmarkMBean {

    private static final Logger logger = LogManager.getLogger(GcBenchmark.class);

    private int threadsNum;
    private int oldObjectsNum = (int) (Runtime.getRuntime().maxMemory() / 1_000L);
    private int objectSize = 50;
    private long iterations = 5_000_000; // 5_000_000 => moins d'1 minute
    private final Random random = new Random();


    public static void main(String... args) {
        Logger rootLogger = LogManager.getRootLogger();
        ConsoleAppender consoleAppender = ConsoleAppender.newBuilder()
                .setTarget(ConsoleAppender.Target.SYSTEM_OUT)
                .build();

        logger.info("About to start the game...");

        try {
            new GcBenchmark().start();
            System.exit(0);
        } catch (Exception e) {
            logger.error(e);
            System.exit(1);
        }
    }

    public GcBenchmark() {
        int processors = Runtime.getRuntime().availableProcessors();
        threadsNum = processors == 1 ? 1 : processors / 2;
    }

    @Override @Timed
    public long start() {
        long start = System.currentTimeMillis();

        ExecutorService executor = Executors.newFixedThreadPool(threadsNum);
        List<GarbageProducer> todo = new ArrayList<>(threadsNum);

        logger.info("Start...");
        for (int i = 0; i < threadsNum; i++) {
            int youngObjectsPerIteration = 100 + random.nextInt(50);
            int oldObjectsPerThread = oldObjectsNum / threadsNum;
            GarbageProducer producer = new GarbageProducer(youngObjectsPerIteration, oldObjectsPerThread);
            todo.add(producer);
        }

        try {
            List<Future<Object>> futures = executor.invokeAll(todo);
            logger.info("invokeAll finished (" + (System.currentTimeMillis() - start) + ")");
            executor.shutdown();
            return System.currentTimeMillis() - start;
        } catch (InterruptedException e) {
            throw new SewaException(e);
        }
    }

    @Override
    public int getThreadsNum() {
        return threadsNum;
    }

    @Override
    public void setThreadsNum(int threadsNum) {
        this.threadsNum = threadsNum;
    }

    @Override
    public int getOldObjectsNum() {
        return oldObjectsNum;
    }

    @Override
    public void setOldObjectsNum(int oldObjectsNum) {
        this.oldObjectsNum = oldObjectsNum;
    }

    @Override
    public int getObjectSize() {
        return objectSize;
    }

    @Override
    public void setObjectSize(int objectSize) {
        this.objectSize = objectSize;
    }

    @Override
    public long getIterations() {
        return iterations;
    }

    @Override
    public void setIterations(long iterations) {
        this.iterations = iterations;
    }

    private static char[] getCharArray(int length) {
        char[] retVal = new char[length];
        Arrays.fill(retVal, '-');
        return retVal;
    }

    /**
     * A garbage producer implementation
     */
    private class GarbageProducer implements Callable<Object> {

        // the fraction of newly created objects that do not become garbage immediately but are stored in the liveList
        private final int fractionLive;
        // the size of the liveList
        private final int myNumLive;

        /**
         * Each GarbageProducer creates objects that become garbage immediately (lifetime=0) and
         * objects that become garbage only after a lifetime>0 which is distributed about an average lifetime.
         * This average lifetime is a function of fractionLive and oldObjectsNum
         *
         * @param fractionLive
         * @param numLive
         */
        GarbageProducer(int fractionLive, int numLive) {
            this.fractionLive = fractionLive;
            this.myNumLive = numLive;
        }

        @Override
        public Object call() {
            long start = System.currentTimeMillis();
            ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
            String threadName = Thread.currentThread().getName();
            logger.info(String.format("Call of thread (%s)", threadName));

            char[] chars = getCharArray(objectSize);
            List<String> keptObjects = new ArrayList<>(myNumLive);
            // initially, the lifeList is filled
            for (int i = 0; i < myNumLive; i++) {
                keptObjects.add(new String(chars));
            }
            for (int j = 0; j < iterations; j++) {
                // create the majority of objects as garbage
                String garbage;
                for (int k = 0; k < fractionLive; k++) {
                    garbage = new String(chars);
                }

                // keep the fraction of objects live by placing them in the list (at a random index)

                try {
                    int index = random.nextInt(myNumLive);
                    keptObjects.set(index, new String(chars));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            long duration = System.currentTimeMillis() - start;
            logger.info(String.format("End of thread (%s) => %s s", threadName, duration / 1000d));
            logger.info(String.format("Conso CPU pour le thread %s : %s %%", threadName, 100 * threadMXBean.getThreadCpuTime(Thread.currentThread().getId()) / duration / 1_000_000));

            return null;
        }
    }

}
