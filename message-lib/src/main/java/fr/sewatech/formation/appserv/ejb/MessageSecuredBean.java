package fr.sewatech.formation.appserv.ejb;

import fr.sewatech.formation.appserv.service.Message;
import fr.sewatech.formation.appserv.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.annotation.Resource;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ejb.*;

@Stateless
@Local(MessageServiceLocal.class)
@Remote(MessageService.class)
@RolesAllowed("sw-ejbuser") // Can be overridden in ejb-jar.xml
public class MessageSecuredBean implements MessageService {
  private static final Logger logger = LogManager.getLogger(MessageSecuredBean.class);

  @Resource
  private SessionContext context;

  @Override
  public Message getMessage(int id) {
    logger.debug("Accessing secured EJB with user " + context.getCallerPrincipal().getName());
    logger.info("Getting message with id " + id);
    return MessageSecuredHolder.get(id);
  }

  @Override
  public int countMessages() {
    return MessageSecuredHolder.count();
  }

  @Override
  public int addMessage(String text) {
    Message message = new Message();
    message.setText(text);
    logger.debug("Added message with text " + text);
    return MessageSecuredHolder.add(message);
  }
}
