package fr.sewatech.formation.appserv.service;

import fr.sewatech.formation.appserv.util.JndiUtil;

import jakarta.jms.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

public final class MessageServiceJms {

    private final Logger logger = LogManager.getLogger(MessageServiceJms.class);

    private final String queueName;
    private Connection connection = null;
    private boolean withJndi = true;

    public static MessageServiceJms unsecured() {
        return new MessageServiceJms("SwQueue");
    }

    public static MessageServiceJms secured() {
        return new MessageServiceJms("SwSecuredQueue");
    }

    private MessageServiceJms(String queueName) {
        this.queueName = queueName;
    }

    public Message getMessage() {
        try {
            logger.debug("Try to receive a message...");
            ObjectMessage message = (ObjectMessage) getJmsMessage();
            logger.debug("Message trouvé : " + message);
            return message == null ? new Message("Aucun message", null) : (Message) message.getObject();
        } catch (Exception e) {
            throw new SewaException("Problème de réception du message", e);
        }
    }

    private jakarta.jms.Message getJmsMessage() throws Exception {
        MessageConsumer consumer = null;
        Session session = null;
        try {
            session = initialize();

            Queue queue;
            if (withJndi) {
                queue = JndiUtil.lookupQueue(queueName);
            } else {
                queue = session.createQueue(queueName);
            }
            consumer = initialize().createConsumer(queue);
            return consumer.receive(10000);
        } finally {
            close(consumer);
            close(session);
            closeConnection();
        }
    }

    public void send(Serializable message) {
        MessageProducer producer = null;
        Session session = null;
        try {
            session = initialize();
            Queue queue;
            if (withJndi) {
                queue = JndiUtil.lookupQueue(queueName);
            } else {
                queue = session.createQueue(queueName);
            }
            producer = session.createProducer(queue);
//            producer.setTimeToLive(30000L);
            jakarta.jms.Message jmsMessage;
            if (message instanceof String) {
                logger.debug("Sending text message via JMS");
                jmsMessage = session.createTextMessage((String) message);
            } else {
                logger.debug("Sending object message via JMS");
                jmsMessage = session.createObjectMessage(message);
            }
            producer.send(jmsMessage);
            logger.debug("Message envoyé : " + message);
        } catch (Exception e) {
            throw new SewaException("Problème d'envoi du message", e);
        } finally {
            close(producer);
            close(session);
            closeConnection();
        }
    }

    /**
     * Enable the JNDI lookup to find the queue
     * @return this
     */
    public MessageServiceJms jndiEnable() {
        withJndi = true;
        return this;
    }

    /**
     * Disable the JNDI lookup, the queue will be retrieved in a provider-specific way. It works fine with ActiveMQ
     * Artemis, but not with HornetQ.
     * @return this
     */
    public MessageServiceJms jndiDisable() {
        withJndi = false;
        return this;
    }

    private Session initialize() throws Exception {
        try {
            ConnectionFactory connectionFactory = JndiUtil.lookupConnectionFactory();

            logger.debug("Initializing connections to JMS server");
            connection = connectionFactory.createConnection();
//            connection = connectionFactory.createConnection("jms", "client");

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
            return session;
        } catch (Exception e) {
            closeConnection();
            throw e;
        }
    }

    private void close(MessageConsumer consumer) {
        if (consumer != null) {
            try {
                logger.debug("Closing consumer");
                consumer.close();
            } catch (JMSException e) {
                logger.warn("Problem when closing the consumer : " + e);
            }
        }
    }

    private void close(MessageProducer producer) {
        if (producer != null) {
            try {
                logger.debug("Closing producer");
                producer.close();
            } catch (JMSException e) {
                logger.warn("Problem when closing the producer : " + e);
            }
        }
    }

    private void close(Session session) {
        if (session != null) {
            try {
                logger.debug("Closing session");
                session.close();
            } catch (JMSException e) {
                logger.warn("Problem when closing the session : " + e);
            }
        }
    }

    public void closeConnection() {
        try {
            if (connection != null) {
                logger.debug("Closing queue connection");
                connection.close();
                connection = null;
            }
        } catch (JMSException e) {
            logger.warn("Problème à la fermeture de la connexion JMS : " + e);
        }
    }

}
