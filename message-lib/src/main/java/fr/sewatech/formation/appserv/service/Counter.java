package fr.sewatech.formation.appserv.service;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class Counter implements Serializable {

    private static final long serialVersionUID = -297393420610043154L;
    private final AtomicInteger number;

    public Counter() {
        this(0);
    }

    public Counter(Integer initial) {
        this.number = new AtomicInteger(initial);
    }

    public Integer increment() {
        return number.incrementAndGet();
    }

    public Integer getNumber() {
        return number.get();
    }

    public Integer reset() {
        number.set(0);
        return 0;
    }

    @Override
    public String toString() {
        return "Count : " + number.get();
    }
}
