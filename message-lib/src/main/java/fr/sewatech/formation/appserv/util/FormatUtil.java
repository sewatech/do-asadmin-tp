package fr.sewatech.formation.appserv.util;

import java.util.Arrays;

public class FormatUtil {

    private static final String JSON_TEXT_TEMPLATE = "\"%s\": \"%s\"";
    private static final String JSON_NUMBER_TEMPLATE = "\"%s\": %s";
    private static final String JSON_NULL_TEMPLATE = "\"%s\": null";
    private static final String TXT_TEMPLATE = "%s: %s";

    public String jsonFormat(String propertyName, Object value) {
        if (value == null) {
            return String.format(JSON_NULL_TEMPLATE, propertyName);
        } else if (value instanceof Number) {
            return String.format(JSON_NUMBER_TEMPLATE, propertyName, value);
        } else {
            return String.format(JSON_TEXT_TEMPLATE, propertyName, value);
        }
    }

    public String arrayJsonFormat(String... values) {
        return '{' +
            format(",", values) +
            '}';

    }

    public String textFormat(String propertyName, Object propertyValue) {
        return String.format(TXT_TEMPLATE, propertyName, propertyValue);
    }

    public String arrayTextFormat(String... values) {
        return format("\n", values);
    }

    private String format(String separator, String... values) {
        String[] formatParts = new String[values.length];
        Arrays.fill(formatParts, "%s");
        return String.format(
            String.join(separator, formatParts),
            values);
    }

}
