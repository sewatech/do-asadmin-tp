package fr.sewatech.formation.appserv.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.JMX;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public final class MBeanUtil {

    private static final String NAME_TEMPLATE = "sewatech.msg:type=%s";
    private static final Logger logger = LogManager.getLogger(MBeanUtil.class);

    private MBeanUtil() {
    }

    public static void registerMBean(Object obj, String type) {
        try {
            MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = new ObjectName(String.format(NAME_TEMPLATE, type));
            mBeanServer.registerMBean(obj, name);
        } catch (MalformedObjectNameException
                | NotCompliantMBeanException
                | InstanceAlreadyExistsException
                | MBeanRegistrationException e) {
            logger.warn("Unable to register MBean" + type, e);
        }
    }

    public static void unregisterMBean(String type) {
        try {
            MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = new ObjectName(String.format(NAME_TEMPLATE, type));
            mBeanServer.unregisterMBean(name);
        } catch (MalformedObjectNameException | MBeanRegistrationException | InstanceNotFoundException e) {
            logger.warn("Unable to unregister MBean" + type, e);
        }
    }


    public static <T> T getMBeanInstance(String type, Class<T> mBeanType) {
        try {
            MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = new ObjectName(String.format(NAME_TEMPLATE, type));
            return JMX.newMBeanProxy(mBeanServer, name, mBeanType);
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException(e);
        }
    }

}
