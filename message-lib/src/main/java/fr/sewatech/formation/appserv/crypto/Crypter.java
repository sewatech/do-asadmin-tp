package fr.sewatech.formation.appserv.crypto;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Alexis Hassler
 */
public class Crypter {
    private final Logger logger = LogManager.getLogger(Crypter.class);

    public static final String KEY = "jaas is the way";
    public static final String ALGO = "Blowfish";

    public String encode(String secret) {
        if (secret == null || secret.isEmpty()) {
            return "";
        }
        try {
            byte[] kbytes = "jaas is the way".getBytes(StandardCharsets.UTF_8);
            SecretKeySpec key = new SecretKeySpec(kbytes, "Blowfish");

            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encoding = cipher.doFinal(secret.getBytes(StandardCharsets.UTF_8));

            return toHex(encoding);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            logger.error("Unable to encrypt the secret", e);
            return "****";
        }
    }

    private String toHex(byte[] encoding) {
        return new BigInteger(encoding).toString(16);
    }

    public String decode(String secret) {
        if (secret == null || secret.isEmpty()) {
            return "";
        }
        try {
            byte[] kbytes = KEY.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec key = new SecretKeySpec(kbytes, ALGO);

            byte[] encoding = fromHex(secret);

            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decode = cipher.doFinal(encoding);
            return new String(decode, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            logger.error("Unable to decrypt the secret", e);
            return "****";
        }
    }

    private byte[] fromHex(String secret) {
        return new BigInteger(secret, 16).toByteArray();
    }

}
