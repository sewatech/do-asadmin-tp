package fr.sewatech.formation.appserv.crypto;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * @author Alexis Hassler
 */
public enum Encoding {
    HEX, BASE64;

    private static final Logger logger = LogManager.getLogger(Encoding.class);

    static final Charset DEFAULT_CHARSET = Charset.forName("ISO-8859-15");

    public static Encoding fromText(String text) {
        if (text == null) {
            return values()[0];
        }
        try {
            return valueOf(text.toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.warn("Unknown encoding : " + text);
            return values()[0];
        }
    }

    public String text() {
        return this.name().toLowerCase();
    }

    public String encode(byte[] arg) {
        switch (this) {
            case HEX:
                return toHex(arg);
            case BASE64:
                return toBase64(arg);
            default:
                return new String(arg, DEFAULT_CHARSET);
        }
    }

    private static String toHex(byte[] arg) {
        if (arg == null) {
            return null;
        }

        char[] hex = "0123456789abcdef".toCharArray();
        StringBuilder sb = new StringBuilder(arg.length << 1);
        for (byte anArg : arg) {
            sb.append(hex[(anArg & 0xf0) >> 4])
                    .append(hex[anArg & 0x0f]);
        }

        return sb.toString();
    }

    private static String toBase64(byte[] arg) {
        return Base64.getEncoder().encodeToString(arg);
    }

}
