package fr.sewatech.formation.appserv.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.management.*;

public class CounterMBean implements DynamicMBean {

    private final Logger logger = LogManager.getLogger(CounterMBean.class);

    private final Counter counter = new Counter();

    @Override
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
        if ("number".equals(attribute)) {
            return counter.increment();
        } else {
            throw new AttributeNotFoundException(attribute);
        }
    }

    @Override
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        throw new AttributeNotFoundException(attribute.getName());
    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        AttributeList attributeList = new AttributeList();
        for (String attribute : attributes) {
            try {
                attributeList.add(new Attribute(attribute, getAttribute(attribute)));
            } catch (AttributeNotFoundException | MBeanException | ReflectionException e) {
                logger.debug("Attribute not found : " + attribute);
            }
        }
        return attributeList;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        for (Attribute attribute : attributes.asList()) {
            try {
                setAttribute(attribute);
            } catch (AttributeNotFoundException | InvalidAttributeValueException | ReflectionException |
                     MBeanException e) {
                logger.debug("Attribute not found: " + attribute.getName());
            }
        }
        return null;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature) {
        if ("reset".equals(actionName)) {
            return counter.reset();
        } else {
            logger.debug("Operation not found: " + actionName);
            return null;
        }
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        MBeanAttributeInfo numberAttribute = new MBeanAttributeInfo("number", "Integer", "number !", true, false, false);
        MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[]{numberAttribute};
        MBeanOperationInfo resetOperation = new MBeanOperationInfo("reset", "Reset the counter", new MBeanParameterInfo[0], "void", MBeanOperationInfo.ACTION);
        MBeanOperationInfo[] operations = {resetOperation};

        return new MBeanInfo("Counter", "Simple counter", attributes, null, operations, null);
    }
}
