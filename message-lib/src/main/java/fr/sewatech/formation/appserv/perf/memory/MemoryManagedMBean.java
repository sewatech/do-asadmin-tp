package fr.sewatech.formation.appserv.perf.memory;

public interface MemoryManagedMBean {

    void plus();
    void minus();

    int getWeight();
    void setWeight(int weight);

    int getCount();
    long getLastDuration();
    long getTotalDuration();
    void resetDuration();

    boolean isLogging();
    void setLogging(boolean logging);

}
