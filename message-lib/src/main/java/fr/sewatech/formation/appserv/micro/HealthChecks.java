package fr.sewatech.formation.appserv.micro;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import java.lang.management.ManagementFactory;

@ApplicationScoped
public class HealthChecks {

    @Produces
    @Liveness
    @Readiness
    public HealthCheck app() {
        return () -> HealthCheckResponse.named("msg.app").up().build();
    }

    @Produces
    @Liveness
    HealthCheck heap() {
        return () -> {
            double freeMemoryRatio = getFreeMemoryRatio();
            return HealthCheckResponse
                    .named("msg.heap-memory")
                    .status(freeMemoryRatio > 0.1)
                    .withData("free-ratio", String.valueOf(freeMemoryRatio))
                    .build();
        };
    }

    private double getFreeMemoryRatio() {
        Runtime runtime = Runtime.getRuntime();
        return 1.0 * runtime.freeMemory() / runtime.maxMemory();
    }

    @Produces
    @Liveness
    HealthCheck cpu() {
        return () -> {
            double cpuLoad = getCpuLoad();
            return HealthCheckResponse
                    .named("msg.cpu")
                    .status(cpuLoad < 0.1)
                    .withData("load", String.valueOf(cpuLoad))
                    .build();
        };
    }

    private double getCpuLoad() {
        return ManagementFactory
                .getOperatingSystemMXBean()
                .getSystemLoadAverage() / 100;
    }
}
