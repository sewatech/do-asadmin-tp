package fr.sewatech.formation.appserv.perf.memory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Gauge;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryManaged implements MemoryManagedMBean {

    private static final Logger logger = LogManager.getLogger(MemoryManaged.class);

    private int weight = 5;
    private boolean logging;

    private final List<HeavyData> list = new ArrayList<>();

    private final AtomicLong lastDuration = new AtomicLong();
    private final AtomicLong totalDuration = new AtomicLong();

    @Override
    @Counted
    public void plus() {
        long start = System.currentTimeMillis();
        list.add(new HeavyData(weight * 100_000).interned());
        lastDuration.set(System.currentTimeMillis() - start);
        totalDuration.addAndGet(lastDuration.get());
        log();
    }

    @Override
    @Counted
    public void minus() {
        long start = System.currentTimeMillis();
        int count = getCount();
        if (count > 0) {
            list.remove(count - 1);
        }
        lastDuration.set(System.currentTimeMillis() - start);
        totalDuration.addAndGet(lastDuration.get());
        log();
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    @Gauge(unit = MetricUnits.NONE)
    public int getCount() {
        return list.size();
    }

    @Override
    @Gauge(unit = MetricUnits.MILLISECONDS)
    public long getLastDuration() {
        return lastDuration.get();
    }

    @Override
    @Gauge(unit = MetricUnits.MILLISECONDS)
    public long getTotalDuration() {
        return totalDuration.get();
    }

    @Override
    @Counted
    public void resetDuration() {
        lastDuration.set(0);
        totalDuration.set(0);
    }

    @Override
    public boolean isLogging() {
        return logging;
    }

    @Override
    public void setLogging(boolean logging) {
        this.logging = logging;
    }

    private void log() {
        if (logging) {
            logger.info(String.format(
                    "count = %s, last duration = %,d ms, total duration = %,d ms",
                    this.getCount(), this.getLastDuration(), this.getTotalDuration()));
            Runtime runtime = Runtime.getRuntime();
            long mega = 1024 * 1024;
            logger.info(String.format(
                    "count = %s, free memory = %,d MB, total memory = %,d MB, max memory = %,d MB",
                    this.getCount(), runtime.freeMemory() / mega, runtime.totalMemory() / mega, runtime.maxMemory() / mega));
        }
    }

}
