package fr.sewatech.formation.appserv.service;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MessageServiceTest {

	@Test
	public void testGetMessage() {
		MessageServiceImpl service = new MessageServiceImpl();
		Message message = service.getMessage(0);
		assertNotNull(message);
		assertNotNull(message.getText());
		assertNotNull(message.getOrigin());

		System.out.println(message.getText() + " from " + message.getOrigin() + " at " + message.getCreatedAsText());
	}

}
