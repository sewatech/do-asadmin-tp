package fr.sewatech.formation.appserv.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CounterTest {

    private Counter counter;

    @Before
    public void setUp() {
        counter = new Counter();
    }

    @Test
    public void testGetNumber() throws Exception {
        assertEquals("First count", 0L, counter.getNumber().longValue());
    }

}
