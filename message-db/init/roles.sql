DROP TABLE sw_roles;

CREATE TABLE sw_roles
  (userid varchar(64),
   roleid varchar(64));

INSERT INTO sw_roles
  (userid, roleid)
VALUES
  ('alexis', 'sw-webuser'),
  ('alexis', 'sw-ejbuser'),
  ('alexis', 'sw-wsuser'),
  ('user0',  'sw-webuser'),
  ('user0',  'sw-ejbuser'),
  ('user1',  'sw-webuser');
