@echo off
title hsqldb - server

rem set JAVA_HOME=c:\java\jdk1.5.0_10
set CLASSPATH=.\lib\hsqldb.jar

rem ** Lance hsqldb sur le port par d�faut (9001) **
call %JAVA_HOME%\bin\java org.hsqldb.Server

rem URL de connexion : jdbc:hsqldb:hsql://localhost/hello
rem login="sa", password=""

pause
