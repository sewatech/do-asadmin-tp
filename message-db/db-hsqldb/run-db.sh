#!/bin/sh
#title hsqldb - server

#export JAVA_HOME=
CLASSPATH=./lib/hsqldb.jar
export CLASSPATH

# ** Lance hsqldb sur le port par défaut (9001) **
java org.hsqldb.Server

# URL de connexion : jdbc:hsqldb:hsql://localhost/hello
# login="sa", password=""

