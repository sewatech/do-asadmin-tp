#!/bin/sh
dir=$(dirname "$0")

#export JAVA_HOME=
CLASSPATH=$dir/lib/h2-1.3.161.jar

# ** Lance H2 **
java -cp $CLASSPATH org.h2.tools.Server -baseDir $dir/data/ -tcp -ifExists &
#
#java -cp "$dir/lib/h2-1.3.161.jar" org.h2.tools.Console "$@"

# driver class=org.h2.Driver
# driver jar=h2-1.x.xxx.jar
# URL de connexion : jdbc:h2:tcp://localhost/sewa-db
# login="sa", password="sa"

