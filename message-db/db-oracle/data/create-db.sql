CREATE USER sa IDENTIFIED BY sapwd;
GRANT ALL PRIVILEGES TO sa;

CREATE TABLE sa.message (id NUMBER(11) NOT NULL, message VARCHAR2(30) NOT NULL, PRIMARY KEY (id));

INSERT INTO sa.message (id, message) VALUES (0, 'Oracle DB est dans la place');
INSERT INTO sa.message (id, message) VALUES (1, 'Hello from Oracle DB');
INSERT INTO sa.message (id, message) VALUES (2, 'No OSS here, it''s Oracle');

commit;
