SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE DATABASE `formation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `formation`;

CREATE TABLE `message` (
  `ID` int(11) NOT NULL,
  `MESSAGE` varchar(30) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `message` (`ID`, `MESSAGE`) VALUES
	(0, 'MySql vous souhaite le bonjour'),
	(1, 'Hello from MySQL');
