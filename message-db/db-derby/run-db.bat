@echo off
title derby - server

set "root_dir=%~dp0%"

set "CLASSPATH=%root_dir%\lib\derby.jar;%root_dir%\lib\derbytools.jar;%root_dir%\lib\derbynet.jar;%CLASSPATH%"

if "%JAVA_HOME%" == "" (
    set "java_path=java"
) else (
    set "java_path=%JAVA_HOME%\bin\java.exe"
)

if "%1" == "debug" (
    echo java_path=%java_path%
    %java_path% -version
    echo ...
)

rem ** Lance derby sur le port 1527, sans accès remote (cf. -h) **
"%java_path%" -Dderby.system.home=%root_dir% org.apache.derby.drda.NetworkServerControl start -h localhost -p 1527  -noSecurityManager

rem driver class=org.apache.derby.jdbc.ClientDriver
rem driver jar=derbyclient.jar
rem URL de connexion : jdbc:derby://localhost:1527/sewadb
rem login="sa", password="sapwd"
